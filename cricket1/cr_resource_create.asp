<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<% option explicit %>

<!-- #Include file="__inc_uniquestr.asp" -->
<!-- #Include file="__inc_dbase.asp" -->

<%
dim dbtbl, query, selID, resource, vUstr, vlastUpdated, nextpage

resource=request.querystring("resource")

setUniqueString
vUstr = session("s_ustr")

vlastUpdated=formatdatetime(now,vblongdate) & " at " & formatdatetime(now,vbshorttime) & " by " & session("s_fullname")

openConnection

if resource="tournaments" then
	query="insert into cr_tournaments (cr_us,cr_tournament,cr_sponsor_ID,cr_lastupdated,cr_owner) values ('"& session("s_ustr") &"','New tournament',1,'"& vlastUpdated &"','"& session("s_owner") &"')"
	set dbtbl=connect.execute(query)	
end if

if resource="teams" then
	query="insert into cr_teams (cr_us,cr_team,cr_lastupdated,cr_owner) values ('"& session("s_ustr") &"','New team','"& vlastUpdated &"','"& session("s_owner") &"')"
	set dbtbl=connect.execute(query)
end if

if resource="venues" then
	query="insert into cr_venues (cr_us,cr_venue,cr_lastupdated,cr_owner) values ('"& session("s_ustr") &"','New venue','"& vlastUpdated &"','"& session("s_owner") &"')"
	set dbtbl=connect.execute(query)
end if

if resource="matchtypes" then
	query="insert into cr_matchtypes (cr_us,cr_matchtype,cr_lastupdated,cr_owner) values ('"& session("s_ustr") &"','New match type','"& vlastUpdated &"','"& session("s_owner") &"')"
	set dbtbl=connect.execute(query)
end if

if resource="gametypes" then
	query="insert into cr_gametypes (cr_us,cr_gametype,cr_lastupdated,cr_owner) values ('"& session("s_ustr") &"','New game type','"& vlastUpdated &"','"& session("s_owner") &"')"
	set dbtbl=connect.execute(query)
end if

if resource="sponsors" then
	query="insert into cr_sponsors (cr_us,cr_sponsor,cr_lastupdated,cr_owner) values ('"& session("s_ustr") &"','New sponsor','"& vlastUpdated &"','"& session("s_owner") &"')"
	set dbtbl=connect.execute(query)
end if

if resource="dismissals" then
	query="insert into cr_dismissals (cr_us,cr_dismissal,cr_lastupdated,cr_owner) values ('"& session("s_ustr") &"','New dismissal','"& vlastUpdated &"','"& session("s_owner") &"')"
	set dbtbl=connect.execute(query)
end if

query="select * from cr_"& resource &" where cr_us='"& session("s_ustr") &"'"
set dbtbl=connect.execute(query)

selID=dbtbl("cr_id")

closeConnection

response.redirect "cr_"& resource &".asp?selID="& selID &"&us="& session("s_ustr")
%>









