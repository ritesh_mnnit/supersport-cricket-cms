<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<% option explicit %>

<!-- #Include file="__inc_uniquestr.asp" -->
<!-- #Include file="__inc_container.asp" -->

<% setUniqueString %>

<%
dim dbtbl, query, dateStart, dateEnd, qTournament, arrYesNo(2)
dateStart=request.querystring("dateStart")
dateEnd=request.querystring("dateEnd")
qTournament=request.querystring("qTournament")

arrYesNo(1)="No"
arrYesNo(2)="Yes"

if not isnumeric(qTournament) then qTournament="" end if
if not isdate(dateStart) then dateStart=date end if
if not isdate(dateEnd) then dateEnd=date end if
%>

<% buildTop %>

  <table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
      <td bgcolor="#F2F2F2" class="text_black_12"><strong>FIXTURES</strong></td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
  </table>
  <table  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td class="text_black_10">
        <strong>
        <select name="f_start_Y" class="input_list" id="f_start_Y">
		<option selected value="<%=year(dateStart)%>"><%=year(dateStart)%></option>
		<% createYears %>
        </select>
        <select name="f_start_M" class="input_list" id="f_start_M">
		<option selected value="<%=month(dateStart)%>"><%=monthname(month(dateStart))%></option>
		<% createMonths %>
        </select>
        <select name="f_start_D" class="input_list" id="f_start_D">
		<option selected value="<%=day(dateStart)%>"><%=day(dateStart)%></option>
		<% createDays %>
        </select> 
        TO
        <select name="f_end_Y" class="input_list" id="f_end_Y">
		<option selected value="<%=year(dateEnd)%>"><%=year(dateEnd)%></option>
		<% createYears %>
        </select>
        <select name="f_end_M" class="input_list" id="f_end_M">
		<option selected value="<%=month(dateEnd)%>"><%=monthname(month(dateEnd))%></option>
		<% createMonths %>
        </select>
        <select name="f_end_D" class="input_list" id="f_end_D">
		<option selected value="<%=day(dateEnd)%>"><%=day(dateEnd)%></option>
		<% createDays %>
        </select>
      </strong></td>
      <td class="text_black_10"><img src="cr_images/_trans.gif" width="10" height="10"></td>
      <td class="text_black_10"><strong>TOURNAMENT</strong></td>
      <td class="text_black_10"><img src="cr_images/_trans.gif" width="10" height="10"></td>
      <td class="text_black_10">
	  <select name="f_tournament" class="input_list" id="f_tournament" style="width:200px">
	
		<% createTournaments %>
	
      </select></td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
  </table>
  <table  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="80" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td bgcolor="#FFFFFF"><input name="btn_fetch" type="button" class="button_green" id="btn_fetch" onClick="MM_goToURL('parent','cr_fixtures.asp?dateStart='+ f_start_Y.value +'-'+ f_start_M.value +'-'+ f_start_D.value +'&dateEnd='+ f_end_Y.value +'-'+ f_end_M.value +'-'+ f_end_D.value +'&qTournament='+ f_tournament.value +'&us=<%=session("s_ustr")%>');return document.MM_returnValue" value="Fetch"></td>
      </tr>
    </table></td>	
    <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    <td><table width="80" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td bgcolor="#FFFFFF"><input name="btn_create" type="button" class="button_blue" value="Create new" onClick="MM_goToURL('parent','cr_fixtures_create.asp?us=<%=session("s_ustr")%>');return document.MM_returnValue"></td>
      </tr>
    </table></td>
  </tr>
</table>

<%
dim qFilter
if not qTournament="" then
	qFilter=" and (tFIX.cr_owner="& session("s_owner") &") and (tFIX.cr_tournament_ID="& qTournament &") and (tFIX.cr_date_start between '"& dateStart &"' and '"& dateEnd &"' or tFIX.cr_date_end between '"& dateStart &"' and '"& dateEnd &"') "
else
	qFilter=" and (tFIX.cr_owner="& session("s_owner") &") and (tFIX.cr_date_start between '"& dateStart &"' and '"& dateEnd &"' or tFIX.cr_date_end between '"& dateStart &"' and '"& dateEnd &"') "
end if

query="select tFIX.*, "& _
		"tTOU.cr_tournament AS vTournament, "& _
		"tGAM.cr_gametype AS vGametype, "& _
		"tMAT.cr_matchtype AS vMatchtype, "& _
		"tVEN.cr_venue AS vVenue, "& _
		"tTEA.cr_team AS vTeamA, "& _
		"tTEB.cr_team AS vTeamB "& _
	"from cr_fixtures tFIX, "& _
		"cr_tournaments tTOU, "& _
		"cr_gametypes tGAM, "& _
		"cr_matchtypes tMAT, "& _
		"cr_venues tVEN, "& _
		"cr_teams tTEA, "& _
		"cr_teams tTEB "& _
	"where ((tTOU.cr_id = tFIX.cr_tournament_ID) "& _
		"and (tGAM.cr_id = tFIX.cr_gametype_ID) "& _
		"and (tMAT.cr_id = tFIX.cr_matchtype_ID) "& _
		"and (tVEN.cr_id = tFIX.cr_venue_ID) "& _
		"and (tTEA.cr_id = tFIX.cr_teamA_ID) "& _
		"and (tTEB.cr_id = tFIX.cr_teamB_ID)) "& _
		qFilter & _
	"order by cr_date_start"

set dbtbl=connect.execute(query)

if not dbtbl.eof then
%>

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="1" cellspacing="1" bgcolor="#CCCCCC">

<%
do until dbtbl.eof
	
	dim vActive, rowCol
	rowCol="#FFFFFF"
	if dbtbl("cr_active")=0 then
		rowCol="#FCFDDF"
	end if
%>

  <tr align="center" bgcolor="#F2F2F2" class="text_black_10">
    <td bgcolor="#F2F2F2"><strong>TOURNAMENT</strong></td>
    <td bgcolor="#F2F2F2"><strong>GAME TYPE</strong></td>
    <td bgcolor="#F2F2F2"><strong>MATCH TYPE</strong></td>
    <td bgcolor="#F2F2F2"><strong>VENUE</strong></td>
    <td bgcolor="#F2F2F2"><strong>TEAM A </strong><a href="cr_scorecard.asp?fixID=<%=dbtbl("cr_id")%>&teamAID=<%=dbtbl("cr_teamA_ID")%>&teamBID=<%=dbtbl("cr_teamB_ID")%>&dateStart=<%=dateStart%>&dateEnd=<%=dateEnd%>&us=<%=session("s_ustr")%>">(Score)</a></td>
    <td bgcolor="#F2F2F2"><strong>TEAM B </strong><a href="cr_scorecard.asp?fixID=<%=dbtbl("cr_id")%>&teamAID=<%=dbtbl("cr_teamB_ID")%>&teamBID=<%=dbtbl("cr_teamA_ID")%>&dateStart=<%=dateStart%>&dateEnd=<%=dateEnd%>&us=<%=session("s_ustr")%>">(Score)</a></td>
  </tr>
  <tr align="center" bgcolor="<%=rowCol%>" class="text_black_10">
    <td bgcolor="<%=rowCol%>"><%=dbtbl("vTournament")%></td>
    <td bgcolor="<%=rowCol%>"><%=dbtbl("vGametype")%></td>
    <td bgcolor="<%=rowCol%>"><%=dbtbl("vMatchtype")%></td>
    <td bgcolor="<%=rowCol%>"><%=dbtbl("vVenue")%></td>
    <td bgcolor="<%=rowCol%>"><%=dbtbl("vteamA")%></td>
    <td bgcolor="<%=rowCol%>"><%=dbtbl("vTeamB")%></td>
  </tr>
  <tr align="center" bgcolor="<%=rowCol%>" class="text_black_10">
    <td rowspan="2" bgcolor="<%=rowCol%>"><table border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td width="25" bgcolor="<%=rowCol%>"><input name="btn_edit" type="button" class="button_yellow" id="btn_edit" value="E" onClick="MM_goToURL('parent','cr_fixtures_edit.asp?fixID=<%=dbtbl("cr_id")%>&dateStart=<%=dateStart%>&dateEnd=<%=dateEnd%>&us=<%=session("s_ustr")%>');return document.MM_returnValue"></td>
        <td width="25" bgcolor="<%=rowCol%>"><input name="btn_delete" type="button" class="button_orange" id="btn_delete" value="D" onClick="confirmDeleteFixture(<%=dbtbl("cr_id")%>,'<%=dateStart%>','<%=dateEnd%>')"></td>
        <td width="25" bgcolor="<%=rowCol%>"><input name="btn_delete" type="button" class="button_blue" id="btn_delete" value="S" onClick="MM_openBrWindow('cr_scorecard_view.asp?fixID=<%=dbtbl("cr_id")%>&teamaID=<%=dbtbl("cr_teamA_ID")%>&teambID=<%=dbtbl("cr_teamB_ID")%>&us=<%=session("s_ustr")%>','','')"></td>
      </tr>
    </table>
    </td>
    <td bgcolor="#F2F2F2"><strong>DATE START</strong></td>
    <td bgcolor="#F2F2F2"><strong>DATE END</strong></td>
    <td bgcolor="#F2F2F2"><strong>DATE RESULT</strong></td>
    <td bgcolor="#F2F2F2"><strong>RESULT ?</strong></td>
    <td bgcolor="#F2F2F2"><strong>ACTIVE ?</strong></td>
  </tr>
  <tr align="center" bgcolor="#FFFFFF" class="text_black_10">
    <td bgcolor="<%=rowCol%>"><%=year(dbtbl("cr_date_start"))%>-<%=month(dbtbl("cr_date_start"))%>-<%=day(dbtbl("cr_date_start"))%><br><%=formatdatetime(timevalue(dbtbl("cr_date_start")),vbshorttime)%></td>
    <td bgcolor="<%=rowCol%>"><%=year(dbtbl("cr_date_end"))%>-<%=month(dbtbl("cr_date_end"))%>-<%=day(dbtbl("cr_date_end"))%></td>
	<% if isdate(dbtbl("cr_date_result")) then %>
	    <td bgcolor="<%=rowCol%>"><%=year(dbtbl("cr_date_result"))%>-<%=month(dbtbl("cr_date_result"))%>-<%=day(dbtbl("cr_date_result"))%></td>
	<% else %>
		<td bgcolor="<%=rowCol%>">&nbsp;</td>
	<% end if %>
    <td bgcolor="<%=rowCol%>"><%=arrYesNo(dbtbl("cr_result")+1)%>, <%=dbtbl("cr_match_result")%></td>
	<td bgcolor="<%=rowCol%>"><%=arrYesNo(dbtbl("cr_active")+1)%></td>
  </tr>
  <tr align="center" bgcolor="#F2F2F2" class="text_black_10">
    <td colspan="6"><strong>MATCH REPORT:</strong> <%=dbtbl("cr_match_report")%></td>
  </tr>
  <tr align="center" bgcolor="#FFFFFF" class="text_black_10">
    <td colspan="6"><img src="cr_images/_trans.gif" width="10" height="10"></td>
  </tr>
  
<%
dbtbl.movenext
loop
%>
  
</table>

<% else %>

  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
  </table>
  <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#000000">
    <tr align="center">
      <td colspan="2" align="left" bgcolor="#FFFFFF" class="text_red_10">No fixtures available</td>
    </tr>
  </table>

<% end if %>

<% buildBottom %>