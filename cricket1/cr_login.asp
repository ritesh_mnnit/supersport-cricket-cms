<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<% option explicit %>

<!-- #Include file="__inc_uniquestr.asp" -->
<!-- #Include file="__inc_dbase.asp" -->

<%
if not request("btn_process")="Login" then
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>SSZ TOOLS : Cricket</title>
<link href="cr_css/cr_general.css" rel="stylesheet" type="text/css">
</head>

<body>

<form name="form_login" method="post">
  <table width="300" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#000000">
    <tr>
      <td align="center" bgcolor="#FFFFFF"><table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td align="center"><img src="cr_images/_trans.gif" width="10" height="20"></td>
          </tr>
          <tr>
            <td align="center" bgcolor="#FFFFFF"><table width="260"  border="0" cellpadding="5" cellspacing="1" bgcolor="#000000">
              <tr>
                <td align="center" bgcolor="#F2F2F2" class="text_black_12"><strong><img src="cr_images/_main.gif" width="39" height="40"><br>
                  CRICKET TOOLS</strong></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="center"><img src="cr_images/_trans.gif" width="10" height="10"></td>
          </tr>
          <tr>
            <td align="center" class="text_black_10">Login name</td>
          </tr>
          <tr>
            <td align="center"><table width="200" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
                <tr>
                  <td align="center" bgcolor="#FFFFFF"><input name="f_username" type="text" class="input_edit_centre"></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td align="center"><img src="cr_images/_trans.gif" width="10" height="10"></td>
          </tr>
          <tr>
            <td align="center" class="text_black_10">Password</td>
          </tr>
          <tr>
            <td align="center"><table width="200" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
                <tr>
                  <td align="center" bgcolor="#FFFFFF"><input name="f_password" type="password" class="input_edit_centre"></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td align="center"><img src="cr_images/_trans.gif" width="10" height="10"></td>
          </tr>
          <tr>
            <td align="center"><table width="100" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
                <tr>
                  <td align="center" bgcolor="#FFFFFF"><input name="btn_process" type="submit" class="button_silver" value="Login"></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td align="center"><img src="cr_images/_trans.gif" width="10" height="20"></td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>
</body>
</html>
<%
else

	if request("f_username")="" or request("f_password")="" then
		response.write "MISSING DATA"
	else

		dim dbtbl, query, vUname, vPword, cnt, illArr(6), repArr(6), illegal
			
		illArr(1)="'"
		illArr(2)="&"
		illArr(3)="/"
		illArr(4)="\"
		illArr(5)=vbCrLf
		illArr(6)=""""
				
		repArr(1)=""
		repArr(2)=""
		repArr(3)=""
		repArr(4)=""
		repArr(5)=""
		repArr(6)=""
	
		vUname=request("f_username")
		vPword=request("f_password")
	
		cnt=1
		do until cnt = 7
		    If InStr(vUname, illArr(cnt)) > 0 Then
		        illegal = True
		        Exit do
		    End If
		    If InStr(vPword, illArr(cnt)) > 0 Then
		        illegal = True
		        Exit do
		    End If
			vUname=replace(vUname,illArr(cnt),repArr(cnt))
			vPword=replace(vPword,illArr(cnt),repArr(cnt))
		    cnt = cnt + 1
		loop
		
		If Len(vUname) > 20 Then
		    illegal = True
		End If
		If Len(vPword) > 20 Then
		    illegal = True
		End If

		openConnection

        If illegal = True Then
            query="select * from cr_users where cr_username='' and cr_password=''"
        Else
            query="select * from cr_users where cr_username='"& vUname &"' and cr_password='"& vPword &"'"
        End If
		set dbtbl=connect.execute(query)

		if dbtbl.eof then
			response.write "INCORRECT DETAILS"
		else
		
			session("s_user") = dbtbl("cr_username")
			session("s_fullname") = dbtbl("cr_fullname")
			session("s_userID") = dbtbl("cr_id")
			session("s_permission") = dbtbl("cr_permission")
			session("s_active") = true
			session("s_owner") = cint(dbtbl("cr_owner"))
			
			Response.Cookies("s_user") = dbtbl("cr_username")
            Response.Cookies("s_user").Expires = DateAdd("D", 1, Date()) 

			setUniqueString

			response.redirect "cr_main.asp?us=" & session("s_ustr")
		
		end if

	end if

end if
%>
