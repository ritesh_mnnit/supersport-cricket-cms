<!-- #Include file="__inc_menus.asp" -->
<!-- #Include file="__inc_dbase.asp" -->
<!-- #Include file="__inc_droplists.asp" -->

<% openConnection %>

<% sub buildTop %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>SSZ TOOLS : Cricket</title>
<link href="cr_css/cr_general.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="cr_scripts/cr_javascript.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_showHideLayers() { //v6.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
//-->
</script>
</head>

<body bgcolor="#E5E5E5">

<iframe src="__inc_nokill.asp" width="1" height="1" frameborder="No" allowtransparency="true"></iframe>

<table  border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td bgcolor="#FFFFFF">

      <table width="900" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
        <tr>
          <td bgcolor="#F2F2F2"><table width="100%"  border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td width="50%"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="39"><div id="layMenu" style="position:absolute; z-index:1; background-color: #E5E5E5; layer-background-color: #E5E5E5; border: 1px none #000000; visibility: hidden; width: 150;" onMouseOver="MM_showHideLayers('layMenu','','show')" onMouseOut="MM_showHideLayers('layMenu','','hide')">
                      <% buildMainMenu %></div>
                      <a href="javascript:;" onMouseOver="MM_showHideLayers('layMenu','','show')"><img src="cr_images/_main.gif" width="39" height="40" border="0"></a></td>
                    <td width="5">&nbsp;</td>
                    <td><strong>
						<span class="text_black_12">SUPERSPORT ZONE</span><br>
						<span class="text_black_10">CRICKET TOOLS</span>
					</strong></td>
                  </tr>
                </table></td>
                <td width="50%" align="right"><strong><span class="text_black_12"><%=session("s_fullname")%>&nbsp;Owner: <%=session("s_owner")%></span></strong><br>
				<span class="text_black_10">If this is not you please <a href="default.asp" class="text_red_10">Log off</a> and re-enter your details</span></td>
              </tr>
          </table></td>
        </tr>
      </table>
      <table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="5"></td>
  </tr>
</table>
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left" valign="top">
    <td>

<% end sub %>

<% sub buildBottom %>

	</td>
  </tr>
</table>
</td>
  </tr>
</table>

<!--</body>
</html>-->

<% end sub %>