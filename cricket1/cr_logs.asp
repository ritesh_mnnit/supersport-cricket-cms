<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<% option explicit %>

<!-- #Include file="__inc_uniquestr.asp" -->
<!-- #Include file="__inc_container.asp" -->

<% setUniqueString %>

<%
dim selID, selTournament, dbtbl, query
selID=request.querystring("selID")

if not isnumeric(selID) or isempty(selID) then
	selID=0
end if
%>

<%
dim queryLOGS, dbtblLOGS
queryLOGS = ""
queryLOGS = queryLOGS & "select "
queryLOGS = queryLOGS & "tLOG.cr_id as vID, "
queryLOGS = queryLOGS & "tLOG.cr_group_pos as vGROUPPOS, "
queryLOGS = queryLOGS & "tLOG.cr_pos as vPOS, "
queryLOGS = queryLOGS & "tLOG.cr_logtype_ID as vTYPEID, "
queryLOGS = queryLOGS & "tLOG.cr_team_ID as vTEAMID, "
queryLOGS = queryLOGS & "tLOG.cr_P as vP, "
queryLOGS = queryLOGS & "tLOG.cr_W as vW, "
queryLOGS = queryLOGS & "tLOG.cr_L as vL, "
queryLOGS = queryLOGS & "tLOG.cr_D as vD, "
queryLOGS = queryLOGS & "tLOG.cr_T as vT, "
queryLOGS = queryLOGS & "tLOG.cr_NR as vNR, "
queryLOGS = queryLOGS & "tLOG.cr_BAT as vBAT, "
queryLOGS = queryLOGS & "tLOG.cr_BOWL as vBOWL, "
queryLOGS = queryLOGS & "tLOG.cr_PEN as vPEN, "
queryLOGS = queryLOGS & "tLOG.cr_MATCH as vMATCH, "
queryLOGS = queryLOGS & "tLOG.cr_PTS as vPTS, "
queryLOGS = queryLOGS & "tLOG.cr_NRR as vNRR, "
queryLOGS = queryLOGS & "tTYP.cr_logtype as vLOGTYPE, "
queryLOGS = queryLOGS & "tTEA.cr_team as vTEAM "
queryLOGS = queryLOGS & "from cr_logs tLOG "
queryLOGS = queryLOGS & "left join cr_logtypes tTYP on tTYP.cr_id = tLOG.cr_logtype_ID "
queryLOGS = queryLOGS & "left join cr_teams tTEA on tTEA.cr_id = tLOG.cr_team_ID "
queryLOGS = queryLOGS & "where (tLOG.cr_tournament_ID = "& selID &") "
queryLOGS = queryLOGS & "order by tLOG.cr_group_pos, tLOG.cr_logtype_ID, tLOG.cr_pos"

'response.Write(queryLOGS)
'response.End()

set dbtblLOGS = connect.execute(queryLOGS)
%>

<% buildTop %>

<form name="form_tournaments" method="post" action="cr_logs_save.asp?selID=<%=selID%>">

  <table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
      <td bgcolor="#F2F2F2" class="text_black_12"><strong>LOGS</strong></td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
	  <select  class="input_list" name="f_tourlist" style="width:300px" onChange="MM_goToURL('parent','cr_logs.asp?selID='+ f_tourlist.value +'&us=<%=session("s_ustr")%>');return document.MM_returnValue">
	
		<option selected value=""></option>
	
		<%
		query="select cr_id, cr_tournament from cr_tournaments where (not cr_id=1) and (cr_owner="& session("s_owner") &") order by cr_tournament"
		set dbtbl=connect.execute(query)
		
		do until dbtbl.eof
		%>
			<% if cint(dbtbl("cr_id")) = cint(selID) then %>
				<option selected value="<%=dbtbl("cr_id")%>"><%=dbtbl("cr_tournament")%></option>
			<% else %>
				<option value="<%=dbtbl("cr_id")%>"><%=dbtbl("cr_tournament")%></option>
			<% end if %>
		<%
		dbtbl.movenext
		loop
		%>
	
      </select>
	  </td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
  </table>
  <table  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="120" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td bgcolor="#FFFFFF">
			<% if selID=0 then %>
			<input name="btn_create" type="button" class="button_blue" value="Create new entry" disabled>
			<% else %>
			<input name="btn_create" type="button" class="button_blue" value="Create new entry" onClick="MM_goToURL('parent','cr_logs_create.asp?selID=<%=selID%>&us= <%=session("s_ustr")%>');return document.MM_returnValue">
			<% end if %>
		</td>
      </tr>
    </table></td>

	<td><img src="cr_images/_trans.gif" width="10" height="10"></td>

	<% if not dbtblLOGS.eof then %>
    <td><table width="120" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td bgcolor="#FFFFFF">
			<input name="btn_save" type="submit" class="button_blue" value="Save this log">
		</td>
      </tr>
    </table></td>
	<% end if %>
	
    </tr>
</table>

<%
if dbtblLOGS.eof then
%>

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="20"></td>
  </tr>
  <tr>
    <td class="text_red_10">NO LOGS FOUND</td>
  </tr>
</table>

<% else %>

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">

<%
dim logType
logType =""

do until dbtblLOGS.eof
%>
	
	<% if not logType = dbtblLOGS("vLOGTYPE") then %>
	<tr class="text_black_10">
		<td width="30" bgcolor="#FFFFFF"><input type="text" class="input_edit_centre" name="f_group_pos_<%=dbtblLOGS("vTYPEID")%>" maxlength="3" value="<%=dbtblLOGS("vGROUPPOS")%>" /></td>
		<td colspan="15" bgcolor="#F2F2F2" class="text_black_12"><strong><%=ucase(dbtblLOGS("vLOGTYPE"))%></strong></td>
	</tr>

	<tr class="text_black_10">
		<td width="20" align="center" bgcolor="#F2F2F2">&nbsp;</td>
		<td width="30" bgcolor="#F2F2F2" class="text_black_10"><strong>POS</strong></td>
		<td width="100" bgcolor="#F2F2F2" class="text_black_10"><strong>LOG TYPE</strong></td>
		<td bgcolor="#F2F2F2" class="text_black_10"><strong>TEAM</strong></td>
		<td width="40" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>P</strong></td>
		<td width="40" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>W</strong></td>
		<td width="40" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>L</strong></td>
		<td width="40" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>D</strong></td>
		
		<td width="40" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>T</strong></td>
		<td width="40" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>NR</strong></td>
		<td width="40" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>BAT</strong></td>
		<td width="40" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>BOWL</strong></td>
		<td width="40" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>PEN</strong></td>
		<td width="40" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>MATCH</strong></td>
		
		<td width="40" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>PTS</strong></td>
		<td width="40" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>NRR</strong></td>
	</tr>
	<% end if %>
	<tr>
		<td width="20" align="center" bgcolor="#F2F2F2"><table width="20" border="0" cellspacing="0" cellpadding="1" bgcolor="#000000">
			<tr>
				<td><input name="btn_delete_<%=dbtblLOGS("vID")%>" type="submit" class="button_orange" value="X" onclick="MM_goToURL('parent','cr_logs_delete.asp?recID=<%=dbtblLOGS("vID")%>&selID=<%=selID%>&us=<%=session("s_ustr")%>');return document.MM_returnValue"></td>
			</tr>
		</table></td>
		<td width="30" bgcolor="#FFFFFF"><input type="text" class="input_edit_left" name="f_pos_<%=dbtblLOGS("vID")%>" maxlength="3" value="<%=dbtblLOGS("vPOS")%>" /></td>
		<td width="100" bgcolor="#FFFFFF">
			<select name="f_logtype_<%=dbtblLOGS("vID")%>" class="input_list">
			
				<%
				query="select cr_id, cr_logtype from cr_logtypes order by cr_id"
				set dbtbl=connect.execute(query)
				
				do until dbtbl.eof
				%>
					<% if cint(dbtbl("cr_id")) = cint(dbtblLOGS("vTYPEID")) then %>
						<option selected value="<%=dbtblLOGS("vTYPEID")%>"><%=dbtblLOGS("vLOGTYPE")%></option>
					<% else %>
						<option value="<%=dbtbl("cr_id")%>"><%=dbtbl("cr_logtype")%></option>
					<% end if %>
				<%
				dbtbl.movenext
				loop
				%>
			</select>
		</td>
		<td bgcolor="#FFFFFF">
			<select name="f_team_<%=dbtblLOGS("vID")%>" class="input_list">
				<option selected value=""></option>
			
				<%
				query="select cr_id, cr_team from cr_teams where (cr_owner="& session("s_owner") &") order by cr_team"
				set dbtbl=connect.execute(query)
				
				do until dbtbl.eof
				%>
					<% if cint(dbtbl("cr_id")) = cint(dbtblLOGS("vTEAMID")) then %>
						<option selected value="<%=dbtblLOGS("vTEAMID")%>"><%=dbtblLOGS("vTEAM")%></option>
					<% else %>
						<option value="<%=dbtbl("cr_id")%>"><%=dbtbl("cr_team")%></option>
					<% end if %>
				<%
				dbtbl.movenext
				loop
				%>
			</select>
		</td>
		<td width="40" align="center" bgcolor="#FFFFFF"><input type="text" class="input_edit_centre" name="f_P_<%=dbtblLOGS("vID")%>" value="<%=dbtblLOGS("vP")%>" /></td>
		<td width="40" align="center" bgcolor="#FFFFFF"><input type="text" class="input_edit_centre" name="f_W_<%=dbtblLOGS("vID")%>" value="<%=dbtblLOGS("vW")%>" /></td>
		<td width="40" align="center" bgcolor="#FFFFFF"><input type="text" class="input_edit_centre" name="f_L_<%=dbtblLOGS("vID")%>" value="<%=dbtblLOGS("vL")%>" /></td>
		<td width="40" align="center" bgcolor="#FFFFFF"><input type="text" class="input_edit_centre" name="f_D_<%=dbtblLOGS("vID")%>" value="<%=dbtblLOGS("vD")%>" /></td>
		<td width="40" align="center" bgcolor="#FFFFFF"><input type="text" class="input_edit_centre" name="f_T_<%=dbtblLOGS("vID")%>" value="<%=dbtblLOGS("vT")%>" /></td>
		<td width="40" align="center" bgcolor="#FFFFFF"><input type="text" class="input_edit_centre" name="f_NR_<%=dbtblLOGS("vID")%>" value="<%=dbtblLOGS("vNR")%>" /></td>
		<td width="40" align="center" bgcolor="#FFFFFF"><input type="text" class="input_edit_centre" name="f_BAT_<%=dbtblLOGS("vID")%>" value="<%=dbtblLOGS("vBAT")%>" /></td>
		<td width="40" align="center" bgcolor="#FFFFFF"><input type="text" class="input_edit_centre" name="f_BOWL_<%=dbtblLOGS("vID")%>" value="<%=dbtblLOGS("vBOWL")%>" /></td>
		<td width="40" align="center" bgcolor="#FFFFFF"><input type="text" class="input_edit_centre" name="f_PEN_<%=dbtblLOGS("vID")%>" value="<%=dbtblLOGS("vPEN")%>" /></td>
		<td width="40" align="center" bgcolor="#FFFFFF"><input type="text" class="input_edit_centre" name="f_MATCH_<%=dbtblLOGS("vID")%>" value="<%=dbtblLOGS("vMATCH")%>" /></td>
		<td width="40" align="center" bgcolor="#FFFFFF"><input type="text" class="input_edit_centre" name="f_PTS_<%=dbtblLOGS("vID")%>" value="<%=dbtblLOGS("vPTS")%>"/></td>
		<td width="40" align="center" bgcolor="#FFFFFF"><input type="text" class="input_edit_centre" name="f_NRR_<%=dbtblLOGS("vID")%>" value="<%=dbtblLOGS("vNRR")%>" /></td>
	</tr>
	
<%
logType=dbtblLOGS("vLOGTYPE")
dbtblLOGS.movenext
loop
%>
	
</table>

<%
queryLOGS = ""
queryLOGS = queryLOGS & "select "
queryLOGS = queryLOGS & "tNOT.cr_notes as vNOTES "
queryLOGS = queryLOGS & "from cr_lognotes tNOT "
queryLOGS = queryLOGS & "where (tNOT.cr_tournament_ID = "& selID &") "
set dbtblLOGS=connect.execute(queryLOGS)
%>

<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC" style="margin-top:20px;">
	<tr>
		<td bgcolor="#F2F2F2" class="text_black_10"><strong>LOG NOTES</strong></td>
	</tr>
	<tr>
		<td bgcolor="#FFFFFF">
		<% if not dbtblLOGS.eof then %>
			<textarea name="f_lognotes" rows="10" class="input_memo_stretch"><%=dbtblLOGS("vNOTES")%></textarea>
		<% else %>
			<textarea name="f_lognotes" rows="10" class="input_memo_stretch"></textarea>
		<% end if %>
		</td>
	</tr>
</table>

<% end if %>

</form>

<% buildBottom %>