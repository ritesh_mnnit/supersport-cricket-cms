<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<% option explicit %>

<!-- #Include file="__inc_uniquestr.asp" -->
<!-- #Include file="__inc_container.asp" -->

<% setUniqueString %>

<%
dim dbtbl, query, dateStart, dateEnd, fixID, teamAID, teamBID, arrYesNo(2), vSR, vECON, innCnt, cnt
dateStart=request.querystring("dateStart")
dateEnd=request.querystring("dateEnd")
fixID=request.querystring("fixID")
teamAID=request.querystring("teamAID")
teamBID=request.querystring("teamBID")

arrYesNo(1)="No"
arrYesNo(2)="Yes"

innCnt=1

if (not isnumeric(fixID)) or (not isnumeric(teamAID)) or (not isnumeric(teamBID)) then
	response.redirect "_error.asp"
end if

if not isdate(dateStart) then dateStart=date end if
if not isdate(dateEnd) then dateEnd=date end if
%>

<%
if (not request("btn_process") = "Save") and (not request("btn_process") = "Create Innings") then
%>

<% buildTop %>

<form name="form_fixture" method="post">

<%
query="select cr_team AS teamBAT from cr_teams where cr_id="& teamAID
set dbtbl=connect.execute(query)
%>

  <table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
      <td bgcolor="#F2F2F2" class="text_black_12"><strong>SCORE CARD : <%=ucase(dbtbl("teamBAT"))%> INNINGS</strong></td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
  </table>
  <table  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
        <tr>
          <td bgcolor="#FFFFFF"><input name="btn_process" type="submit" class="button_blue" value="Create Innings"></td>
        </tr>
      </table></td>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
      <td><table width="100" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
          <tr>
            <td bgcolor="#FFFFFF"><input name="btn_process" type="submit" class="button_yellow" value="Save"></td>
          </tr>
      </table></td>

      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
      <td><table width="100" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
        <tr>
          <td bgcolor="#FFFFFF"><input name="btn_back" type="button" class="button_green" id="btn_back" onClick="MM_goToURL('parent','cr_fixtures.asp?dateStart=<%=dateStart%>&dateEnd=<%=dateEnd%>&us=<%=session("s_ustr")%>');return document.MM_returnValue" value="Back to Fixtures"></td>
        </tr>
      </table></td>
    </tr>
  </table>

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
  </tr>
</table>

<%
do until innCnt=3

query ="select tBAT.*, "& _
	   "tTEA.cr_team AS vTeamBAT, "& _
	   "tTEB.cr_team AS vTeamBOWL, "& _
	   "tDIS.cr_dismissal AS vDismissal, "& _
	   "tDIS.cr_id AS vDismissalID, "& _
	   "tFIE.cr_player AS vFielder, "& _
	   "tFIE.cr_id AS vFielderID, "& _
	   "tBOW.cr_player AS vBowled, "& _
	   "tBOW.cr_id AS vBowledID, "& _
	   "tPLA.cr_player AS vBatsman, "& _
	   "tPLA.cr_id AS vBatsmanID "& _
  "from cr_score_batting tBAT "& _
  	   "inner join cr_teams tTEA on tBAT.cr_team_bat_ID=tTEA.cr_id "& _
	   "inner join cr_teams tTEB on tBAT.cr_team_field_ID=tTEB.cr_id "& _
	   "left join cr_dismissals tDIS on tBAT.cr_dismissal_ID=tDIS.cr_id "& _
	   "left join cr_players tFIE on tBAT.cr_fielder_out_ID=tFIE.cr_id "& _
	   "left join cr_players tBOW on tBAT.cr_bowled_player_ID=tBOW.cr_id "& _
	   "left join cr_players tPLA on tBAT.cr_player_ID=tPLA.cr_id "& _
  "where (tBAT.cr_inning="& innCnt &" and tBAT.cr_fixture_ID="& fixID & " and tBAT.cr_team_bat_ID="& teamAID &") "& _
  "order by tBAT.cr_inning, tBAT.cr_number"
		
set dbtbl=connect.execute(query)

if not dbtbl.eof then
%>

<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
  	<% if cint(dbtbl("cr_inning"))=2 then %>
	    <td bgcolor="#F2F2F2" class="text_red_10"><strong>INNINGS # <%=dbtbl("cr_inning")%></strong>&nbsp;&nbsp;<a href="cr_inning_delete.asp?innID=<%=dbtbl("cr_inning")%>&fixID=<%=fixID%>&teamAID=<%=teamAID%>&teamBID=<%=teamBID%>&dateStart=<%=dateStart%>&dateEnd=<%=dateEnd%>&us=<%=session("s_ustr")%>">(Delete)</a></td>
	<% else %>
		<td bgcolor="#F2F2F2" class="text_red_10"><strong>INNINGS # <%=dbtbl("cr_inning")%></strong></td>
	<% end if %>
  </tr>
</table>

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="1"></td>
  </tr>
</table>

<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr bgcolor="#F2F2F2" class="text_black_10">
  	<td width="20" align="center" class="text_black_10"><img src="cr_images/_trans.gif" width="10" height="10"></td>
  	<td width="20" align="center" class="text_black_10"><strong>#</strong></td>
    <td bgcolor="#F2F2F2"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
	    <td width="110"><table width="100" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
          <tr>
            <td><input name="btn_process" type="button" class="button_silver" id="btn_process" value="NEW BATSMAN" onClick="MM_goToURL('parent','cr_batsman_create.asp?fixID=<%=fixID%>&teamAID=<%=teamAID%>&teamBID=<%=teamBID%>&vCurrInning=<%=dbtbl("cr_inning")%>&dateStart=<%=dateStart%>&dateEnd=<%=dateEnd%>&us=<%=session("s_ustr")%>');return document.MM_returnValue"></td>
          </tr>
        </table></td>
        <td class="text_black_10"><strong>BATSMAN</strong></td>
      </tr>
    </table></td>
    <td bgcolor="#F2F2F2" class="text_black_10"><strong>DISMISSAL</strong></td>
    <td bgcolor="#F2F2F2" class="text_black_10"><strong>FIELDER</strong></td>
    <td bgcolor="#F2F2F2" class="text_black_10"><strong>BOWLER</strong></td>
    <td width="25" align="center" class="text_black_10"><strong>R</strong></td>
    <td width="25" align="center" class="text_black_10"><strong>M</strong></td>
    <td width="25" align="center" class="text_black_10"><strong>B</strong></td>
    <td width="25" align="center" class="text_black_10"><strong>4s</strong></td>
    <td width="25" align="center" class="text_black_10"><strong>6s</strong></td>
    <td width="50" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>SR</strong></td>
  </tr>
  
  <%
  cnt=0
  do until dbtbl.eof
  
	if dbtbl("cr_balls")=0 then
		vSR = (cdbl(dbtbl("cr_runs")) / 1) * 100
	else
	  	vSR = (cdbl(dbtbl("cr_runs")) / cdbl(dbtbl("cr_balls"))) * 100
	end if
  %>
  
  <tr bgcolor="#FFFFFF" class="text_black_10">
  	<td width="20" align="center">
	<% if cnt > 0 then %>
	<table width="100%"  border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td><input name="btn_delbat" type="button" class="button_orange" id="btn_delbat" onClick="MM_goToURL('parent','cr_batsman_delete.asp?batID=<%=dbtbl("cr_id")%>&fixID=<%=fixID%>&teamAID=<%=teamAID%>&teamBID=<%=teamBID%>&dateStart=<%=dateStart%>&dateEnd=<%=dateEnd%>&us=<%=session("s_ustr")%>');return document.MM_returnValue" value="X"></td>
      </tr>
    </table>
	<% else %>
	&nbsp;
	<% end if %>
	</td>
  	<td width="20" align="center"><input name="f_number_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_number_<%=dbtbl("cr_id")%>" maxlength="3" style width="10px" value="<%=dbtbl("cr_number")%>"></td>
    <td bgcolor="#FFFFFF"><select name="f_batsman_<%=dbtbl("cr_id")%>" class="input_list" id="f_batsman_<%=dbtbl("cr_id")%>">
	<option selected value="<%=dbtbl("vBatsmanID")%>"><%=dbtbl("vBatsman")%></option>
	
		<%
		dim queryBATSMAN, dbtblBATSMAN
		queryBATSMAN="select tPLA.* from cr_players tPLA where cr_team_ID="& teamAID &" order by cr_player"
		set dbtblBATSMAN=connect.execute(queryBATSMAN)
		
		do until dbtblBATSMAN.eof
		%>
			<option value="<%=dbtblBATSMAN("cr_id")%>"><%=dbtblBATSMAN("cr_player")%></option>
		<%
		dbtblBATSMAN.movenext
		loop
		%>

    </select></td>
	
    <td bgcolor="#FFFFFF"><select name="f_dismissal_<%=dbtbl("cr_id")%>" class="input_list" id="f_dismissal_<%=dbtbl("cr_id")%>">
	<option selected value="<%=dbtbl("vDismissalID")%>"><%=dbtbl("vDismissal")%></option>
	
		<%
		dim queryDISMISSAL, dbtblDISMISSAL
		queryDISMISSAL="select * from cr_dismissals where cr_active=1 order by cr_dismissal"
		set dbtblDISMISSAL=connect.execute(queryDISMISSAL)
		
		do until dbtblDISMISSAL.eof
		%>
			<option value="<%=dbtblDISMISSAL("cr_id")%>"><%=dbtblDISMISSAL("cr_dismissal")%></option>
		<%
		dbtblDISMISSAL.movenext
		loop
		%>
	
    </select></td>
	
	<td bgcolor="#FFFFFF"><select name="f_fielder_<%=dbtbl("cr_id")%>" class="input_list" id="f_fielder_<%=dbtbl("cr_id")%>">
	<option selected value="<%=dbtbl("vFielderID")%>"><%=dbtbl("vFielder")%></option>
	
		<%
		dim queryFIELDER, dbtblFIELDER
		queryFIELDER="select tPLA.* from cr_players tPLA where cr_team_ID="& teamBID &" order by cr_player"
		set dbtblFIELDER=connect.execute(queryFIELDER)
		
		do until dbtblFIELDER.eof
		%>
			<option value="<%=dbtblFIELDER("cr_id")%>"><%=dbtblFIELDER("cr_player")%></option>
		<%
		dbtblFIELDER.movenext
		loop
		%>
	
    </select></td>
	
    <td bgcolor="#FFFFFF"><select name="f_bowled_<%=dbtbl("cr_id")%>" class="input_list" id="f_bowled_<%=dbtbl("cr_id")%>">
	<option selected value="<%=dbtbl("vBowledID")%>"><%=dbtbl("vBowled")%></option>
	
		<%
		dim queryBOWLED, dbtblBOWLED
		queryBOWLED="select tPLA.* from cr_players tPLA where cr_team_ID="& teamBID &" order by cr_player"
		set dbtblBOWLED=connect.execute(queryBOWLED)
		
		do until dbtblBOWLED.eof
		%>
			<option value="<%=dbtblBOWLED("cr_id")%>"><%=dbtblBOWLED("cr_player")%></option>
		<%
		dbtblBOWLED.movenext
		loop
		%>
	
    </select></td>
    <td width="25" align="center"><input name="f_runs_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_runs_<%=dbtbl("cr_id")%>" maxlength="3" style width="10px" value="<%=dbtbl("cr_runs")%>"></td>
    <td width="25" align="center"><input name="f_minutes_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_minutes_<%=dbtbl("cr_id")%>" maxlength="3" style width="10px" value="<%=dbtbl("cr_minutes")%>"></td>
    <td width="25" align="center"><input name="f_balls_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_balls_<%=dbtbl("cr_id")%>" maxlength="3" style width="10px" value="<%=dbtbl("cr_balls")%>"></td>
    <td width="25" align="center"><input name="f_4s_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_4s_<%=dbtbl("cr_id")%>" maxlength="3" style width="10px" value="<%=dbtbl("cr_fours")%>"></td>
    <td width="25" align="center"><input name="f_6s_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_6s_<%=dbtbl("cr_id")%>" maxlength="3" style width="10px" value="<%=dbtbl("cr_sixes")%>"></td>
    <td width="50" align="center" bgcolor="#F2F2F2"><strong><%=formatnumber(vSR,2)%></strong></td>
  </tr>
  
  <%
  cnt=cnt+1
  dbtbl.movenext
  loop
  %>

</table>

<%
query ="select tEXT.* "& _
  "from cr_score_extras tEXT "& _
  	   "inner join cr_teams tTEA on tEXT.cr_team_bat_ID=tTEA.cr_id "& _
	   "inner join cr_teams tTEB on tEXT.cr_team_field_ID=tTEB.cr_id "& _
  "where (tEXT.cr_inning="& innCnt &" and tEXT.cr_fixture_ID="& fixID & " and tEXT.cr_team_bat_ID="& teamAID &") "
  
set dbtbl=connect.execute(query)

dim totExtras
totExtras=0

totExtras= cint(dbtbl("cr_byes")) + cint(dbtbl("cr_legbyes")) + cint(dbtbl("cr_noballs")) + cint(dbtbl("cr_wides")) + cint(dbtbl("cr_penalty"))
%>

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="1"></td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr bgcolor="#F2F2F2" class="text_black_10">
    <td align="right" bgcolor="#F2F2F2"><strong>EXTRAS</strong></td>
    <td width="25" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>B</strong></td>
    <td width="25" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>LB</strong></td>
    <td width="25" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>NB</strong></td>
    <td width="25" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>W</strong></td>
    <td width="25" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>P</strong></td>
    <td width="50" align="center" class="text_black_10">&nbsp;</td>
  </tr>
  <tr bgcolor="#FFFFFF" class="text_black_10">
    <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
    <td width="25" align="center" bgcolor="#FFFFFF"><input name="f_xbyes_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_xbyes_<%=dbtbl("cr_id")%>" maxlength="6" style width="15px" value="<%=dbtbl("cr_byes")%>"></td>
    <td width="25" align="center" bgcolor="#FFFFFF"><input name="f_xlegbyes_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_xlegbyes_<%=dbtbl("cr_id")%>" maxlength="3" style width="10px" value="<%=dbtbl("cr_legbyes")%>"></td>
    <td width="25" align="center" bgcolor="#FFFFFF"><input name="f_xnoballs_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_xnoballs_<%=dbtbl("cr_id")%>" maxlength="3" style width="10px" value="<%=dbtbl("cr_noballs")%>"></td>
    <td width="25" align="center" bgcolor="#FFFFFF"><input name="f_xwides_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_xwides_<%=dbtbl("cr_id")%>" maxlength="3" style width="10px" value="<%=dbtbl("cr_wides")%>"></td>
    <td width="25" align="center" bgcolor="#FFFFFF"><input name="f_xpenalty_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_xpenalty_<%=dbtbl("cr_id")%>" maxlength="3" style width="10px" value="<%=dbtbl("cr_penalty")%>"></td>
    <td width="50" align="center" bgcolor="#FFFFFF"><strong><%=totExtras%></strong></td>
  </tr>
</table>

<%
query ="select tFOW.*, tPLA.cr_player AS vBatsman, tPLA.cr_id AS vBatsmanID "& _
  "from cr_score_fow tFOW "& _
  	   "inner join cr_teams tTEA on tFOW.cr_team_bat_ID=tTEA.cr_id "& _
	   "inner join cr_teams tTEB on tFOW.cr_team_field_ID=tTEB.cr_id "& _
	   "left join cr_players tPLA on tFOW.cr_batsman_ID=tPLA.cr_id "& _
  "where (tFOW.cr_inning="& innCnt &" and tFOW.cr_fixture_ID="& fixID & " and tFOW.cr_team_bat_ID="& teamAID &") "
  
set dbtbl=connect.execute(query)
%>

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="1"></td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td bgcolor="#F2F2F2" class="text_black_10"><strong>FALL OF WICKETS </strong></td>
  </tr>
</table>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="1"></td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr bgcolor="#F2F2F2" class="text_black_10">
    <td width="20" align="center" bgcolor="#F2F2F2" class="text_black_10">&nbsp;</td>
    <td align="left" bgcolor="#F2F2F2" class="text_black_10"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="110"><table width="100" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
            <tr>
              <td><input name="btn_process" type="button" class="button_silver" id="btn_process" value="NEW BATSMAN" onClick="MM_goToURL('parent','cr_fow_create.asp?fixID=<%=fixID%>&teamAID=<%=teamAID%>&teamBID=<%=teamBID%>&vCurrInning=<%=dbtbl("cr_inning")%>&dateStart=<%=dateStart%>&dateEnd=<%=dateEnd%>&us=<%=session("s_ustr")%>');return document.MM_returnValue"></td>
            </tr>
        </table></td>
        <td class="text_black_10"><strong>BATSMAN</strong></td>
      </tr>
    </table></td>
    <td width="25" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>W</strong></td>
    <td width="25" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>TT</strong></td>
    <td width="25" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>O</strong></td>
  </tr>
  
  <%
  cnt=0
  do until dbtbl.eof
  %>
  
  <tr bgcolor="#FFFFFF" class="text_black_10">
    <td width="20" align="center" bgcolor="#FFFFFF">
	<% if cnt > 0 then %>
	<table width="100%"  border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td><input name="btn_delbat" type="button" class="button_orange" id="btn_delbat" onClick="MM_goToURL('parent','cr_fow_delete.asp?batID=<%=dbtbl("cr_id")%>&fixID=<%=fixID%>&teamAID=<%=teamAID%>&teamBID=<%=teamBID%>&dateStart=<%=dateStart%>&dateEnd=<%=dateEnd%>&us=<%=session("s_ustr")%>');return document.MM_returnValue" value="X"></td>
      </tr>
    </table>
	<% else %>
	&nbsp;
	<% end if %>
	</td>
	<td align="left" bgcolor="#FFFFFF"><select name="f_fowbatsman_<%=dbtbl("cr_id")%>" class="input_list" id="f_fowbatsman_<%=dbtbl("cr_id")%>">
	<option selected value="<%=dbtbl("vBatsmanID")%>"><%=dbtbl("vBatsman")%></option>
	
		<%
		queryBATSMAN="select tPLA.* from cr_players tPLA where cr_team_ID="& teamAID &" order by cr_player"
		set dbtblBATSMAN=connect.execute(queryBATSMAN)
		
		do until dbtblBATSMAN.eof
		%>
			<option value="<%=dbtblBATSMAN("cr_id")%>"><%=dbtblBATSMAN("cr_player")%></option>
		<%
		dbtblBATSMAN.movenext
		loop
		%>

    </select></td>
    <td width="25" align="center" bgcolor="#FFFFFF"><input name="f_fowwicket_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="fow_fwicket_<%=dbtbl("cr_id")%>" maxlength="6" style width="15px" value="<%=dbtbl("cr_wicket")%>"></td>
    <td width="25" align="center" bgcolor="#FFFFFF"><input name="f_fowteamtotal_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_fowteamtotal_<%=dbtbl("cr_id")%>" maxlength="4" style width="20px" value="<%=dbtbl("cr_teamtotal")%>"></td>
    <td width="25" align="center" bgcolor="#FFFFFF"><input name="f_fowovers_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_fowovers_<%=dbtbl("cr_id")%>" maxlength="4" style width="10px" value="<%=dbtbl("cr_overs")%>"></td>
  </tr>
  
  <%
  cnt=cnt+1
  dbtbl.movenext
  loop
  %>
  
</table>

<%
query ="select tBOW.*, "& _
	   "tTEA.cr_team AS vTeamBAT, "& _
	   "tTEB.cr_team AS vTeamBOWL, "& _
	   "tBLR.cr_player AS vBowler, "& _
	   "tBLR.cr_id AS vBowlerID "& _
  "from cr_score_bowling tBOW "& _
  	   "inner join cr_teams tTEA on tBOW.cr_team_bat_ID=tTEA.cr_id "& _
	   "inner join cr_teams tTEB on tBOW.cr_team_field_ID=tTEB.cr_id "& _
	   "left join cr_players tBLR on tBOW.cr_player_ID=tBLR.cr_id "& _
  "where (tBOW.cr_inning="& innCnt &" and tBOW.cr_fixture_ID="& fixID & " and tBOW.cr_team_bat_ID="& teamAID &") "& _
  "order by tBOW.cr_inning, tBOW.cr_number"
		
set dbtbl=connect.execute(query)
%>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="1"></td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td bgcolor="#F2F2F2" class="text_black_10"><strong>BOWLING</strong></td>
  </tr>
</table>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="1"></td>
  </tr>
</table>


<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr bgcolor="#F2F2F2" class="text_black_10">
    <td width="20" align="center" class="text_black_10"><img src="cr_images/_trans.gif" width="10" height="10"></td>
  	<td width="20" align="center" class="text_black_10"><strong>#</strong></td>
    <td bgcolor="#F2F2F2"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="110"><table width="100" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
              <tr>
                <td><input name="btn_process" type="submit" class="button_silver" id="btn_process" value="NEW BOWLER" onClick="MM_goToURL('parent','cr_bowler_create.asp?fixID=<%=fixID%>&teamAID=<%=teamAID%>&teamBID=<%=teamBID%>&vCurrInning=<%=dbtbl("cr_inning")%>&dateStart=<%=dateStart%>&dateEnd=<%=dateEnd%>&us=<%=session("s_ustr")%>');return document.MM_returnValue"></td>
              </tr>
          </table></td>
          <td class="text_black_10"><strong>BOWLER</strong></td>
        </tr>
    </table></td>
    <td width="30" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>O</strong></td>
    <td width="25" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>M</strong></td>
    <td width="25" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>R</strong></td>
    <td width="25" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>W</strong></td>
	<td width="25" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>NB</strong></td>
	<td width="25" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>WB</strong></td>
	<td width="25" align="center" bgcolor="#F2F2F2" class="text_black_10"><strong>DB</strong></td>
    <td width="50" align="center" class="text_black_10"><strong>ECON</strong></td>
  </tr>
  
<%
cnt=0

dim totBalls, strLen

do until dbtbl.eof

	strLen=len(formatnumber(dbtbl("cr_overs"),1))

	if cdbl(dbtbl("cr_overs"))=0 then
		vECON = 0
	else
		totBalls = cint(mid(formatnumber(dbtbl("cr_overs"),1) , 1, strLen-2)*6) + cint(mid(formatnumber(dbtbl("cr_overs"),1) , strLen))
		vECON = ((cdbl(dbtbl("cr_runs")) / cdbl(totBalls)) * 6)
	end if
%>
  
  <tr bgcolor="#FFFFFF" class="text_black_10">
    <td width="20" align="center">
	<% if cnt > 0 then %>
	<table width="100%"  border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td><input name="btn_delbowl" type="button" class="button_orange" id="btn_delbowl" onClick="MM_goToURL('parent','cr_bowler_delete.asp?bowlID=<%=dbtbl("cr_id")%>&fixID=<%=fixID%>&teamAID=<%=teamAID%>&teamBID=<%=teamBID%>&dateStart=<%=dateStart%>&dateEnd=<%=dateEnd%>&us=<%=session("s_ustr")%>');return document.MM_returnValue" value="X"></td>
      </tr>
    </table>
	<% else %>
	&nbsp;
	<% end if %>
	</td>
  	<td width="20" align="center"><input name="f_bowlnumber_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_bowlnumber_<%=dbtbl("cr_id")%>" maxlength="3" style width="10px" value="<%=dbtbl("cr_number")%>"></td>
    <td bgcolor="#FFFFFF"><select name="f_bowler_<%=dbtbl("cr_id")%>" class="input_list" id="f_bowler_<%=dbtbl("cr_id")%>">
	<option selected value="<%=dbtbl("vBowlerID")%>"><%=dbtbl("vBowler")%></option>
	
		<%
		dim queryBOWLERNAME, dbtblBOWLERNAME
		queryBOWLERNAME="select tPLA.* from cr_players tPLA where cr_team_ID="& teamBID &" order by cr_player"
		set dbtblBOWLERNAME=connect.execute(queryBOWLERNAME)
		
		do until dbtblBOWLERNAME.eof
		%>
			<option value="<%=dbtblBOWLERNAME("cr_id")%>"><%=dbtblBOWLERNAME("cr_player")%></option>
		<%
		dbtblBOWLERNAME.movenext
		loop
		%>
	
    </select></td>
    <td width="30" align="center" bgcolor="#FFFFFF"><input name="f_bowlovers_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_bowlovers_<%=dbtbl("cr_id")%>" maxlength="6" style width="15px" value="<%=dbtbl("cr_overs")%>"></td>
    <td width="25" align="center" bgcolor="#FFFFFF"><input name="f_bowlmaidens_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_bowlmaidens_<%=dbtbl("cr_id")%>" maxlength="3" style width="10px" value="<%=dbtbl("cr_maidens")%>"></td>
    <td width="25" align="center" bgcolor="#FFFFFF"><input name="f_bowlruns_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_bowlruns_<%=dbtbl("cr_id")%>" maxlength="3" style width="10px" value="<%=dbtbl("cr_runs")%>"></td>
    <td width="25" align="center" bgcolor="#FFFFFF"><input name="f_bowlwickets_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_bowlwickets_<%=dbtbl("cr_id")%>" maxlength="3" style width="10px" value="<%=dbtbl("cr_wickets")%>"></td>
	<td width="25" align="center" bgcolor="#FFFFFF"><input name="f_bowlnoball_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_bowlnoball_<%=dbtbl("cr_id")%>" maxlength="3" style width="10px" value="<%=dbtbl("cr_noball")%>"></td>
    <td width="25" align="center" bgcolor="#FFFFFF"><input name="f_bowlwideball_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_bowlwideball_<%=dbtbl("cr_id")%>" maxlength="3" style width="10px" value="<%=dbtbl("cr_wide")%>"></td>
	<td width="25" align="center" bgcolor="#FFFFFF"><input name="f_bowldeadball_<%=dbtbl("cr_id")%>" type="text" class="input_edit_centre_small" id="f_bowldeadball_<%=dbtbl("cr_id")%>" maxlength="3" style width="10px" value="<%=dbtbl("cr_dead")%>"></td>
    <td width="50" align="center" bgcolor="#F2F2F2"><strong><%=formatnumber(vECON,2)%></strong></td>
  </tr>
  
<%
cnt=cnt+1
dbtbl.movenext
loop
%>
  
</table>

<% if innCnt=1 then %>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr bgcolor="#000000">
    <td><img src="cr_images/_trans.gif" width="10" height="5"></td>
  </tr>
</table>
<% end if %>

<%
end if

innCnt=innCnt+1
loop
%>

</form>

<% buildBottom %>

<%
else
	dim queryWRITE, dbtblWRITE

	'#########################################################################################################################################################
	'SAVE BATTING SCORES
	'#########################################################################################################################################################
	
	dim batBatsman, batFielder, batDismissal, batBowled, batRuns, batMins, batBalls, batFours, batSixes, batNumber
	
	query="select * from cr_score_batting where cr_fixture_id="& fixID &" and cr_team_bat_ID="& teamAID
	set dbtbl=connect.execute(query)
	
	do until dbtbl.eof
	
		if request("f_batsman_"& dbtbl("cr_id"))="" then batBatsman=0 else batBatsman=request("f_batsman_"& dbtbl("cr_id")) end if
		if request("f_dismissal_"& dbtbl("cr_id"))="" then batDismissal=0 else batDismissal=request("f_dismissal_"& dbtbl("cr_id")) end if
		if request("f_fielder_"& dbtbl("cr_id"))="" then batFielder=0 else batFielder=request("f_fielder_"& dbtbl("cr_id")) end if
		if request("f_bowled_"& dbtbl("cr_id"))="" then batBowled=0 else batBowled=request("f_bowled_"& dbtbl("cr_id")) end if
		if (not isnumeric(request("f_runs_"& dbtbl("cr_id")))) then batRuns=0 else batRuns=request("f_runs_"& dbtbl("cr_id")) end if
		if (not isnumeric(request("f_minutes_"& dbtbl("cr_id")))) then batMins=0 else batMins=request("f_minutes_"& dbtbl("cr_id")) end if
		if (not isnumeric(request("f_balls_"& dbtbl("cr_id")))) then batBalls=0 else batBalls=request("f_balls_"& dbtbl("cr_id")) end if
		if (not isnumeric(request("f_4s_"& dbtbl("cr_id")))) then batFours=0 else batFours=request("f_4s_"& dbtbl("cr_id")) end if
		if (not isnumeric(request("f_6s_"& dbtbl("cr_id")))) then batSixes=0 else batSixes=request("f_6s_"& dbtbl("cr_id")) end if
		if (not isnumeric(request("f_number_"& dbtbl("cr_id")))) then batNumber=0 else batNumber=request("f_number_"& dbtbl("cr_id")) end if
		
		queryWRITE="update cr_score_batting set cr_number="& batNumber &", "& _
											   "cr_player_ID="& batBatsman &", "& _
											   "cr_dismissal_ID="& batDismissal &", "& _ 
											   "cr_fielder_out_ID="& batFielder &", "& _ 
											   "cr_bowled_player_ID="& batBowled &", "& _ 
											   "cr_runs="& batRuns &", "& _ 
											   "cr_minutes="& batMins &", "& _ 
											   "cr_balls="& batBalls &", "& _ 
											   "cr_fours="& batFours &", "& _ 
											   "cr_sixes="& batSixes &" "& _ 
											   "where cr_id="& dbtbl("cr_id")
										 
		set dbtblWRITE=connect.execute(queryWRITE)
	
	dbtbl.movenext
	loop
	
	'#########################################################################################################################################################
	'SAVE EXTRAS
	'#########################################################################################################################################################
	
	dim extByes, extLegbyes, extNoballs, extWides, extPenalty
	
	query="select * from cr_score_extras where cr_fixture_id="& fixID &" and cr_team_bat_ID="& teamAID
	set dbtbl=connect.execute(query)
	
	do until dbtbl.eof
		
		if (not isnumeric(request("f_xbyes_"    & dbtbl("cr_id")))) then extByes=0    else extByes=    request("f_xbyes_"    & dbtbl("cr_id")) end if
		if (not isnumeric(request("f_xlegbyes_" & dbtbl("cr_id")))) then extLegbyes=0 else extLegbyes= request("f_xlegbyes_" & dbtbl("cr_id")) end if
		if (not isnumeric(request("f_xnoballs_" & dbtbl("cr_id")))) then extNoballs=0 else extNoballs= request("f_xnoballs_" & dbtbl("cr_id")) end if
		if (not isnumeric(request("f_xwides_"   & dbtbl("cr_id")))) then extWides=0   else extWides=   request("f_xwides_"   & dbtbl("cr_id")) end if
		if (not isnumeric(request("f_xpenalty_" & dbtbl("cr_id")))) then extPenalty=0 else extPenalty= request("f_xpenalty_" & dbtbl("cr_id")) end if
		
		queryWRITE="update cr_score_extras set cr_byes="     & extByes    &", "& _
											  "cr_legbyes="  & extLegbyes &", "& _
											  "cr_noballs="  & extNoballs &", "& _ 
											  "cr_wides="    & extWides   &", "& _ 
											  "cr_penalty="  & extPenalty &" "& _ 
											  "where cr_id=" & dbtbl("cr_id")
										 
		set dbtblWRITE=connect.execute(queryWRITE)
	
	dbtbl.movenext
	loop
	
	'#########################################################################################################################################################
	'SAVE FALL OF WICKETS
	'#########################################################################################################################################################
	
	dim fowWicket, fowTeamtotal, fowBatsman, fowOvers
	
	query="select * from cr_score_fow where cr_fixture_id="& fixID &" and cr_team_bat_ID="& teamAID
	set dbtbl=connect.execute(query)
	
	do until dbtbl.eof
		
		if (not isnumeric(request("f_fowwicket_"    & dbtbl("cr_id")))) then fowWicket=0    else fowWicket=    request("f_fowwicket_"    & dbtbl("cr_id")) end if
		if (not isnumeric(request("f_fowteamtotal_" & dbtbl("cr_id")))) then fowTeamtotal=0 else fowTeamtotal= request("f_fowteamtotal_" & dbtbl("cr_id")) end if
		if (not isnumeric(request("f_fowbatsman_"   & dbtbl("cr_id")))) then fowBatsman=0   else fowBatsman=   request("f_fowbatsman_"   & dbtbl("cr_id")) end if
		if (not isnumeric(request("f_fowovers_"     & dbtbl("cr_id")))) then fowOvers=0     else fowOvers=     request("f_fowovers_"     & dbtbl("cr_id")) end if
		
		queryWRITE="update cr_score_fow set cr_wicket="        & fowWicket    &", "& _
											  "cr_teamtotal="  & fowTeamtotal &", "& _
											  "cr_batsman_ID=" & fowBatsman &", "& _ 
											  "cr_overs="      & fowOvers   &" "& _ 
											  "where cr_id="   & dbtbl("cr_id")
										 
		set dbtblWRITE=connect.execute(queryWRITE)
	
	dbtbl.movenext
	loop
	
	'#########################################################################################################################################################
	'SAVE BOWLING SCORES
	'#########################################################################################################################################################
	
	dim bowNumber, bowBowler, bowOvers, bowMaidens, bowRuns, bowWickets, bowWideball, bowNoball, bowDeadball
	
	query="select * from cr_score_bowling where cr_fixture_id="& fixID &" and cr_team_bat_ID="& teamAID
	set dbtbl=connect.execute(query)
	
	do until dbtbl.eof
	
		if request("f_bowler_"& dbtbl("cr_id"))="" then bowBowler=0 else bowBowler=request("f_bowler_"& dbtbl("cr_id")) end if
		
		if (not isnumeric(request("f_bowlovers_"   & dbtbl("cr_id")))) then bowOvers=0    else bowOvers=    request("f_bowlovers_"    & dbtbl("cr_id")) end if
		if (not isnumeric(request("f_bowlmaidens_" & dbtbl("cr_id")))) then bowMaidens=0  else bowMaidens=  request("f_bowlmaidens_"  & dbtbl("cr_id")) end if
		if (not isnumeric(request("f_bowlruns_"    & dbtbl("cr_id")))) then bowRuns=0     else bowRuns=     request("f_bowlruns_"     & dbtbl("cr_id")) end if
		if (not isnumeric(request("f_bowlwickets_" & dbtbl("cr_id")))) then bowWickets=0  else bowWickets=  request("f_bowlwickets_"  & dbtbl("cr_id")) end if
		if (not isnumeric(request("f_bowlwideball_"& dbtbl("cr_id")))) then bowWideball=0 else bowWideball= request("f_bowlwideball_" & dbtbl("cr_id")) end if
		if (not isnumeric(request("f_bowlnoball_"  & dbtbl("cr_id")))) then bowNoball=0   else bowNoball=   request("f_bowlnoball_"   & dbtbl("cr_id")) end if
		if (not isnumeric(request("f_bowldeadball_"& dbtbl("cr_id")))) then bowDeadball=0 else bowDeadball= request("f_bowldeadball_" & dbtbl("cr_id")) end if
		if (not isnumeric(request("f_bowlnumber_"  & dbtbl("cr_id")))) then bowNumber=0   else bowNumber=   request("f_bowlnumber_"   & dbtbl("cr_id")) end if
		
		queryWRITE="update cr_score_bowling set cr_number="    & bowNumber   &", "& _
											   "cr_player_ID=" & bowBowler   &", "& _
											   "cr_overs="     & bowOvers    &", "& _ 
											   "cr_maidens="   & bowMaidens  &", "& _ 
											   "cr_runs="      & bowRuns     &", "& _ 
											   "cr_wickets="   & bowWickets  &", "& _ 
											   "cr_wide="      & bowWideball &", "& _ 
											   "cr_noball="    & bowNoball   &", "& _ 
											   "cr_dead="      & bowDeadball &" "& _ 
											   "where cr_id="  & dbtbl("cr_id")
										 
		set dbtblWRITE=connect.execute(queryWRITE)
	
	dbtbl.movenext
	loop
	
	
	if request("btn_process") = "Create Innings" then
		response.redirect "cr_innings_create.asp?fixID="&fixID&"&teamAID="&teamAID&"&teamBID="&teamBID&"&dateStart="&dateStart&"&dateEnd="&dateEnd&"&us="&session("s_ustr")
	end if
	
	if request("btn_process") = "Save" then
		response.redirect "cr_scorecard.asp?fixID="& fixID &"&teamAID="& teamAID &"&teamBID="& teamBID &"&dateStart="& dateStart &"&dateEnd="& dateEnd &"&us="& session("s_ustr")
	end if
	
end if
%>


