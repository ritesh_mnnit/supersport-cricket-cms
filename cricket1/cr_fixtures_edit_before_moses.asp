<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<% option explicit %>

<!-- #Include file="__inc_uniquestr.asp" -->
<!-- #Include file="__inc_container.asp" -->

<% setUniqueString %>

<%
dim dbtbl, query, dateStart, dateEnd, fixID, arrYesNo(2)
dateStart=request.querystring("dateStart")
dateEnd=request.querystring("dateEnd")
fixID=request.querystring("fixID")

arrYesNo(1)="No"
arrYesNo(2)="Yes"

if not isnumeric(fixID) then
	response.redirect "_error.asp"
end if

if not isdate(dateStart) then dateStart=date end if
if not isdate(dateEnd) then dateEnd=date end if
%>

<%
if not request("btn_process") = "Save" then
%>

<% buildTop %>

<form name="form_fixture" method="post">

  <table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
      <td bgcolor="#F2F2F2" class="text_black_12"><strong>EDIT FIXTURE</strong></td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
  </table>
  <table  border="0" cellspacing="0" cellpadding="0">
    <tr>

      <td><table width="80" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
          <tr>
            <td bgcolor="#FFFFFF"><input name="btn_process" type="submit" class="button_yellow" value="Save"></td>
          </tr>
      </table></td>

      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
      <td><table width="80" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
        <tr>
          <td bgcolor="#FFFFFF"><input name="btn_back" type="button" class="button_green" id="btn_back" value="Back" onClick="history.back()"></td>
        </tr>
      </table></td>
    </tr>
  </table>
<%
query="select tFIX.*, "& _
		"tTOU.cr_tournament AS vTournament, "& _
		"tGAM.cr_gametype AS vGametype, "& _
		"tMAT.cr_matchtype AS vMatchtype, "& _
		"tVEN.cr_venue AS vVenue, "& _
		"tTEA.cr_team AS vTeamA, "& _
		"tTEB.cr_team AS vTeamB "& _
	"from cr_fixtures tFIX, "& _
		"cr_tournaments tTOU, "& _
		"cr_gametypes tGAM, "& _
		"cr_matchtypes tMAT, "& _
		"cr_venues tVEN, "& _
		"cr_teams tTEA, "& _
		"cr_teams tTEB "& _
	"where ((tTOU.cr_id = tFIX.cr_tournament_ID) "& _
		"and (tGAM.cr_id = tFIX.cr_gametype_ID) "& _
		"and (tMAT.cr_id = tFIX.cr_matchtype_ID) "& _
		"and (tVEN.cr_id = tFIX.cr_venue_ID) "& _
		"and (tTEA.cr_id = tFIX.cr_teamA_ID) "& _
		"and (tTEB.cr_id = tFIX.cr_teamB_ID)) "& _
		"and (tFIX.cr_id="& fixID &") "& _
	"order by cr_date_start"

set dbtbl=connect.execute(query)

if not dbtbl.eof then
	dim vResult, active
%>

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>ID NUMBER</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10"><%=dbtbl("cr_id") %></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>TOURNAMENT</strong></td>
    <td height="20" bgcolor="#FFFFFF"><select name="f_tournament" class="input_list" id="f_tournament" style="width:300px">
	<option selected value="<%=dbtbl("cr_tournament_ID")%>"><%=dbtbl("vTournament")%></option>
	<% createTournaments %>
    </select></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>GAME TYPE</strong></td>
    <td height="20" bgcolor="#FFFFFF"><select name="f_gametype" class="input_list" id="f_gametype" style="width:300px">
	<option selected value="<%=dbtbl("cr_gametype_ID")%>"><%=dbtbl("vGametype")%></option>
	<% createGametypes %>
    </select></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>MATCH TYPE</strong></td>
    <td height="20" bgcolor="#FFFFFF"><select name="f_matchtype" class="input_list" id="f_matchtype" style="width:300px">
	<option selected value="<%=dbtbl("cr_matchtype_ID")%>"><%=dbtbl("vMatchtype")%></option>
	<% createMatchtypes %>
    </select></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>VENUE</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10"><select name="f_venue" class="input_list" id="f_venue" style="width:300px">
	<option selected value="<%=dbtbl("cr_venue_ID")%>"><%=dbtbl("vVenue")%></option>
	<% createVenues %>
    </select></td>
  </tr>
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>TEAM A</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10"><select name="f_teamA" class="input_list" id="f_teamA" style="width:300px">
	<option selected value="<%=dbtbl("cr_teamA_ID")%>"><%=dbtbl("vTeamA")%></option>
	<% createTeams %>
    </select></td>
  </tr>
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>TEAM B</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10"><select name="f_teamB" class="input_list" id="f_teamB" style="width:300px">
	<option selected value="<%=dbtbl("cr_teamB_ID")%>"><%=dbtbl("vTeamB")%></option>
	<% createTeams %>
    </select></td>
  </tr>
    <tr>
        <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>Channel 1</strong></td>
        <td height="20" bgcolor="#FFFFFF" class="text_black_10"><select name="f_channel1" class="input_list" id="f_channel1" style="width:300px">
	    <option selected value="<%=dbtbl("channel1")%>"><%=dbtbl("channel1")%></option>
	    <% createChannels %>
        </select></td>
  </tr>
    <tr>
        <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>Channel 2</strong></td>
        <td height="20" bgcolor="#FFFFFF" class="text_black_10"><select name="f_channel2" class="input_list" id="f_channel2" style="width:300px">
	    <option selected value="<%=dbtbl("channel2")%>"><%=dbtbl("channel2")%></option>
	    <% createChannels %>
        </select></td>
  </tr>
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>DATE START</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10">
      <table  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <select name="f_start_Y" class="input_list" id="f_start_Y" onchange="verifyDate(f_start_Y.value , f_start_M.value , f_start_D.value , false)">
              <option selected value="<%=year(dbtbl("cr_date_start"))%>"><%=year(dbtbl("cr_date_start"))%></option>
              <% createYears %>
            </select>
            <select name="f_start_M" class="input_list" id="f_start_M" onchange="verifyDate(f_start_Y.value , f_start_M.value , f_start_D.value , false)">
              <option selected value="<%=month(dbtbl("cr_date_start"))%>"><%=monthname(month(dbtbl("cr_date_start")))%></option>
              <% createMonths %>
            </select>
            <select name="f_start_D" class="input_list" id="f_start_D" onchange="verifyDate(f_start_Y.value , f_start_M.value , f_start_D.value , false)">
              <option selected value="<%=day(dbtbl("cr_date_start"))%>"><%=day(dbtbl("cr_date_start"))%></option>
              <% createDays %>
            </select>
			</td>
          <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
          <td class="text_black_10"><strong>TIME</strong></td>
          <td>&nbsp;</td>
          <td><select name="f_start_hrs" class="input_list" id="f_start_hrs">
				<option selected value="<%=hour(dbtbl("cr_date_start"))%>"><%=hour(dbtbl("cr_date_start"))%></option>
		  <% createTime("hrs") %>
          </select></td>
          <td width="10" align="center"><strong>:</strong></td>
          <td><select name="f_start_min" class="input_list" id="f_start_min">
				<option selected value="<%=minute(dbtbl("cr_date_start"))%>"><%=minute(dbtbl("cr_date_start"))%></option>
		  <% createTime("min") %>
          </select></td>
        </tr>
      </table>
      </td>
  </tr>
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>DATE END</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10">
      <table  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <select name="f_end_Y" class="input_list" id="f_end_Y" onChange="verifyDate(f_end_Y.value , f_end_M.value , f_end_D.value , false)">
              <option selected value="<%=year(dbtbl("cr_date_end"))%>"><%=year(dbtbl("cr_date_end"))%></option>
              <% createYears %>
            </select>
            <select name="f_end_M" class="input_list" id="f_end_M" onChange="verifyDate(f_end_Y.value , f_end_M.value , f_end_D.value , false)">
              <option selected value="<%=month(dbtbl("cr_date_end"))%>"><%=monthname(month(dbtbl("cr_date_end")))%></option>
              <% createMonths %>
            </select>
            <select name="f_end_D" class="input_list" id="f_end_D" onChange="verifyDate(f_end_Y.value , f_end_M.value , f_end_D.value , false)">
              <option selected value="<%=day(dbtbl("cr_date_end"))%>"><%=day(dbtbl("cr_date_end"))%></option>
              <% createDays %>
            </select>
          </td>
          <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
          </tr>
      </table>
      </td>
  </tr>
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>DATE RESULT</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10">
      <table  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <select name="f_res_Y" class="input_list" id="f_res_Y" onChange="verifyDate(f_res_Y.value , f_res_M.value , f_res_D.value , false)">
              <option value""></option>
              <% if isdate(dbtbl("cr_date_result")) then %>
              <option selected value="<%=year(dbtbl("cr_date_result"))%>"><%=year(dbtbl("cr_date_result"))%></option>
              <% end if %>
              <% createYears %>
            </select>
            <select name="f_res_M" class="input_list" id="f_res_M" onChange="verifyDate(f_res_Y.value , f_res_M.value , f_res_D.value , false)">
              <option value""></option>
              <% if isdate(dbtbl("cr_date_result")) then %>
              <option selected value="<%=month(dbtbl("cr_date_result"))%>"><%=monthname(month(dbtbl("cr_date_result")))%></option>
              <% end if %>
              <% createMonths %>
            </select>
            <select name="f_res_D" class="input_list" id="f_res_D" onChange="verifyDate(f_res_Y.value , f_res_M.value , f_res_D.value , false)">
              <option value""></option>
              <% if isdate(dbtbl("cr_date_result")) then %>
              <option selected value="<%=day(dbtbl("cr_date_result"))%>"><%=day(dbtbl("cr_date_result"))%></option>
              <% end if %>
              <% createDays %>
            </select>
          </td>
          <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
          </tr>
      </table>
      </td>
  </tr>
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>RESULT ?</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10">      <table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="50"><select name="f_result" class="input_list" id="f_result">
            <option selected value="<%=dbtbl("cr_result")%>"><%=arrYesNo(dbtbl("cr_result")+1)%></option>
            <% createYesNo %>
          </select></td>
          <td><table width="100%"  border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
            <tr>
              <td bgcolor="#FFFFFF"><input name="f_matchresult" type="text" class="input_edit_left" id="f_matchresult" value="<%=dbtbl("cr_match_result") %>" maxlength="50"></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><img src="cr_images/_trans.gif" width="10" height="10"></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="98" class="text_black_10"><strong>SHORT VERSION</strong></td>
        <td width="798"><table width="100%"  border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
          <tr>
            <td bgcolor="#FFFFFF"><input name="f_matchresult_short" type="text" class="input_edit_left" id="f_matchresult_short" value="<%=dbtbl("cr_match_result_short") %>" maxlength="52"></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
  	<td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>MATCH REPORT</strong></td>
  	<td height="20" bgcolor="#FFFFFF" class="text_black_10"><input name="f_matchreport" type="text" class="input_edit_left" id="f_matchreport" value="<%=dbtbl("cr_match_report") %>" maxlength="250"></td>
  	</tr>
<tr>
  	<td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>MATCH SCORECARD</strong></td>
  	<td height="20" bgcolor="#FFFFFF" class="text_black_10"><input name="f_scorecard" type="text" class="input_edit_left" id="f_scorecard" value="<%=dbtbl("cr_scorecard") %>" maxlength="250"></td>
  	</tr>
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>ACTIVE ?</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10"><select name="f_active" class="input_list" id="f_active">
	<option selected value="<%=dbtbl("cr_active")%>"><%=arrYesNo(dbtbl("cr_active")+1)%></option>
	<% createYesNo %>
    </select></td>
  </tr>
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>LAST UPDATED</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10"><%=dbtbl("cr_lastupdated") %></td>
  </tr>
</table>

  <% else %>
        
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
</table>
  <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#000000">
    <tr align="center">
      <td colspan="2" align="left" bgcolor="#FFFFFF" class="text_red_10">No data</td>
    </tr>
  </table>

<% end if %>

</form>

<% buildBottom %>

<%
else
	dim dStart, dEnd, dResult
	
	dStart	= request("f_start_Y") & "-" & request("f_start_M") & "-"& request("f_start_D") & " " & request("f_start_hrs") & ":"& request("f_start_min")
	dEnd	= request("f_end_Y") & "-" & request("f_end_M") & "-"& request("f_end_D")
	dResult	= request("f_res_Y") & "-" & request("f_res_M") & "-"& request("f_res_D")
	
	if  (request("f_tournament")="" or _
		request("f_gametype")="" or _
		request("f_matchtype")="" or _
		request("f_venue")="" or _
		request("f_teamA")="" or _
		request("f_teamB")="" or _
		request("f_result")="" or _
		request("f_active")="") or ((not isdate(dStart)) or (not isdate(dEnd))) or (len(dResult) > 2 and not isdate(dResult)) then
		
		response.write "DETAILS ERROR<br>make sure you have values in all fields. If no result date exists, you should leave it completely blank."
	else
	
		dim vlastUpdated, vMatchresult, vMatchresultShort, vMatchReport
		vlastUpdated=formatdatetime(now,vblongdate) & " at " & formatdatetime(now,vbshorttime) & " by " & session("s_fullname")
		
		vMatchresult=replace(request("f_matchresult"),"'","`")
		vMatchresultShort=replace(request("f_matchresult_short"),"'","`")
		vMatchReport=replace(request("f_matchreport"),"'","`")
		
		if isdate(dResult) then
			query="update cr_fixtures set cr_tournament_ID="& request("f_tournament") & _
										", cr_gametype_ID="& request("f_gametype") & _
										", cr_matchtype_ID="& request("f_matchtype") & _
										", cr_venue_ID="& request("f_venue") & _
										", cr_teamA_ID="& request("f_teamA") & _
										", cr_teamB_ID="& request("f_teamB") & _
										", cr_result="& request("f_result") & _
										", cr_active="& request("f_active") & _
                                        ", channel1='"& request("f_channel1") & _
                                        "', channel2='"& request("f_channel2") & _
										"', cr_match_result='"& vMatchresult & _
										"', cr_match_result_short='"& vMatchresultShort & _
										"', cr_match_report='"& vMatchReport & _
										"', cr_date_start='"& dStart & _
										"', cr_date_end='"& dEnd & _
										"', cr_date_result='"& dResult & _
										"', cr_lastupdated='"& vLastUpdated & _
                                        "', cr_scorecard='"& request("f_scorecard") & _
										"' where cr_id="& fixID
		else

			query="update cr_fixtures set cr_tournament_ID="& request("f_tournament") & _
										", cr_gametype_ID="& request("f_gametype") & _
										", cr_matchtype_ID="& request("f_matchtype") & _
										", cr_venue_ID="& request("f_venue") & _
										", cr_teamA_ID="& request("f_teamA") & _
										", cr_teamB_ID="& request("f_teamB") & _
										", cr_result="& request("f_result") & _
										", cr_active="& request("f_active") & _
                                        ", channel1='"& request("f_channel1") & _
                                        "', channel2='"& request("f_channel2") & _
										"', cr_match_result='"& vMatchresult & _
										"', cr_match_result_short='"& vMatchresultShort & _
										"', cr_match_report='"& vMatchReport & _
										"', cr_date_start='"& dStart & _
										"', cr_date_end='"& dEnd & _
										"', cr_date_result=null" & _
										", cr_lastupdated='"& vLastUpdated & _
                                        "', cr_scorecard='"& request("f_scorecard") & _
										"' where cr_id="& fixID
		end if

		set dbtbl=connect.execute(query)

		response.redirect "cr_fixtures.asp?dateStart="& dateStart & "&dateEnd=" & dateEnd & "&us=" & session("s_ustr")

	end if

end if
%>





