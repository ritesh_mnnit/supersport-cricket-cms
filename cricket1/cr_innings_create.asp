<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<% option explicit %>

<!-- #Include file="__inc_uniquestr.asp" -->
<!-- #Include file="__inc_dbase.asp" -->

<%
dim vUstr, dbtbl, query, dateStart, dateEnd, fixID, teamAID, teamBID, vNextInning, vLastUpdated
dateStart=request.querystring("dateStart")
dateEnd=request.querystring("dateEnd")
fixID=request.querystring("fixID")
teamAID=request.querystring("teamAID")
teamBID=request.querystring("teamBID")

if (not isnumeric(fixID)) or (not isnumeric(teamAID)) or (not isnumeric(teamBID)) then
	response.redirect "_error.asp"
end if

setUniqueString
vUstr = session("s_ustr")

vlastUpdated=formatdatetime(now,vblongdate) & " at " & formatdatetime(now,vbshorttime) & " by " & session("s_fullname")

openConnection

query="select count(distinct cr_inning) as vNOR from cr_score_batting where cr_fixture_ID="& fixID &" and cr_team_bat_ID="& teamAID
set dbtbl=connect.execute(query)

if int(dbtbl("vNOR")) < 2 then

	vNextInning = int(dbtbl("vNOR")) + 1

	query="insert into cr_score_batting (cr_us,cr_inning,cr_fixture_ID,cr_team_bat_ID,cr_team_field_ID,cr_lastupdated,cr_owner) values ('"& vUstr &"',"& vNextInning &","& fixID &","& teamAID &","& teamBID &",'"& vlastUpdated &"','"& session("s_owner") &"')"
	set dbtbl=connect.execute(query)
	
	query="insert into cr_score_extras (cr_us,cr_inning,cr_fixture_ID,cr_team_bat_ID,cr_team_field_ID,cr_lastupdated,cr_owner) values ('"& vUstr &"',"& vNextInning &","& fixID &","& teamAID &","& teamBID &",'"& vlastUpdated &"','"& session("s_owner") &"')"
	set dbtbl=connect.execute(query)
	
	query="insert into cr_score_fow (cr_us,cr_inning,cr_fixture_ID,cr_team_bat_ID,cr_team_field_ID,cr_lastupdated,cr_owner) values ('"& vUstr &"',"& vNextInning &","& fixID &","& teamAID &","& teamBID &",'"& vlastUpdated &"','"& session("s_owner") &"')"
	set dbtbl=connect.execute(query)

	query="insert into cr_score_bowling (cr_us,cr_inning,cr_fixture_ID,cr_team_bat_ID,cr_team_field_ID,cr_lastupdated,cr_owner) values ('"& vUstr &"',"& vNextInning &","& fixID &","& teamAID &","& teamBID &",'"& vlastUpdated &"','"& session("s_owner") &"')"
	set dbtbl=connect.execute(query)

end if

closeConnection

response.redirect "cr_scorecard.asp?fixID="& fixID &"&teamAID="& teamAID &"&teamBID="& teamBID &"&dateStart="& dateStart &"&dateEnd="& dateEnd &"&us="& session("s_ustr")
%>
