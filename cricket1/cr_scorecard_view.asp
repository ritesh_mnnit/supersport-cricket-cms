<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<% option explicit %>

<%
If Request.Cookies("s_user") = "" Then
    Response.Redirect("default.asp")
End If
        
dim connect
set connect = Server.CreateObject("ADODB.Connection")
'connect.open("PROVIDER=SQLOLEDB;SERVER=ltc1;UID=tlns;PWD=12345;DATABASE=ssz_cricket")
connect.open("PROVIDER=SQLOLEDB;SERVER=sql.supersport.co.za;UID=schoolcricket2;PWD=9K13ztzdRucd;DATABASE=schoolcrickett")
%>

<%
dim fixID, teamAID, teamBID, teamsArr(2), inningsArr(2), inningsCnt, teamsCnt
dim dbtblBAT, queryBAT
dim dbtblEXT, queryEXT
dim dbtblFOW, queryFOW
dim dbtblBOW, queryBOW

dim totRuns, totExtras, inningsTotalRuns, strikeRate

fixID=request.querystring("fixID")
teamAID=request.querystring("teamAID")
teamBID=request.querystring("teamBID")

if not isnumeric(fixID) or fixID="" then fixID=-1 end if
if not isnumeric(teamAID) or teamAID="" then teamAID=-1 end if
if not isnumeric(teamBID) or teamBID="" then teamBID=-1 end if

teamsArr(1)=teamAID
teamsArr(2)=teamBID

inningsArr(1)=1
inningsArr(2)=2

teamsCnt=1
inningsCnt=1
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Cricket scorecard</title>
<link href="cr_css/cr_scorecard.css" rel="stylesheet" type="text/css">
</head>

<body>


<%
do until teamsCnt=3

	inningsCnt=1
	
	do until inningsCnt=3

		queryBAT="select tBAT.*, "& _
				"tTEA.cr_team as vTeamA, "& _
				"tPLA.cr_player as vBatsman, "& _
				"tDIS.cr_dismissal as vDismissal, "& _
				"tDIS.cr_dismissal_sn as vDismissalShort, "& _
				"tFIE.cr_player as vFielderOut, "& _
				"tBOW.cr_player as vBowlerOut "& _
				"from cr_score_batting tBAT "& _
				"inner join cr_teams tTEA on tTEA.cr_id = tBAT.cr_team_bat_ID "& _
				"left join cr_players tPLA on tPLA.cr_id = tBAT.cr_player_ID "& _
				"left join cr_dismissals tDIS on tDIS.cr_id = tBAT.cr_dismissal_ID "& _
				"left join cr_players tFIE on tFIE.cr_id = tBAT.cr_fielder_out_ID "& _
				"left join cr_players tBOW on tBOW.cr_id = tBAT.cr_bowled_player_ID "& _
				"where (tBAT.cr_fixture_ID='"& fixID &"' and tBAT.cr_team_bat_ID='"& teamsArr(teamsCnt) &"' and cr_inning='"& inningsArr(inningsCnt) &"') order by tBAT.cr_number"

		set dbtblBAT=connect.execute(queryBAT)

		if not dbtblBAT.eof then
		
		inningsTotalRuns=0
%>

<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td bgcolor="#CCCCCC" class="txt_black_12"><strong><%=ucase(dbtblBAT("vTeamA"))%></strong> Innings <%=dbtblBAT("cr_inning")%></td>
  </tr>
</table>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="1"></td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td bgcolor="#CCCCCC" class="txt_black_10"><strong>BATTING</strong></td>
  </tr>
</table>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="1"></td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td align="left" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>BATSMAN</strong></td>
    <td align="left" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>DISMISSAL</strong></td>
    <td align="left" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>FIELDER</strong></td>
    <td align="left" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>BOWLER</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>R</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>M</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>B</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>4s</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>6s</strong></td>
    <td width="40" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>SR</strong></td>
  </tr>
  <%
  totRuns=0
  do until dbtblBAT.eof
  	
	strikeRate=0
	
	if not dbtblBAT("cr_runs")=0 and not dbtblBAT("cr_balls")=0 then
		strikeRate=formatnumber(((cdbl(dbtblBAT("cr_runs")) / cdbl(dbtblBAT("cr_balls"))) * 100),2)
	end if
  %>
  <tr>
    <td align="left" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblBAT("vBatsman")%></td>
    <td align="left" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblBAT("vDismissal")%></td>
    <td align="left" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblBAT("vFielderOut")%></td>
    <td align="left" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblBAT("vBowlerOut")%></td>
    <td width="25" align="center" bgcolor="#000000" class="txt_white_10"><strong><%=dbtblBAT("cr_runs")%></strong></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblBAT("cr_minutes")%></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblBAT("cr_balls")%></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblBAT("cr_fours")%></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblBAT("cr_sixes")%></td>
    <td width="40" align="center" bgcolor="#F2F2F2" class="txt_black_10"><strong><%=strikeRate%></strong></td>
  </tr>
  <%
  totRuns=totRuns + cint(dbtblBAT("cr_runs"))
  dbtblBAT.movenext
  loop
  %>
</table>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="1"></td>
  </tr>
</table>

<%
totExtras=0

queryEXT="select tEXT.* from cr_score_extras tEXT where (tEXT.cr_fixture_ID='"& fixID &"' and tEXT.cr_team_bat_ID='"& teamsArr(teamsCnt) &"' and cr_inning='"& inningsArr(inningsCnt) &"')"
set dbtblEXT=connect.execute(queryEXT)
		
if not dbtblEXT.eof then
totExtras = totExtras + cint(dbtblEXT("cr_byes")) + cint(dbtblEXT("cr_legbyes")) + cint(dbtblEXT("cr_noballs")) + cint(dbtblEXT("cr_wides")) + cint(dbtblEXT("cr_penalty"))
%>

<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td align="right" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>EXTRAS</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>B</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>LB</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>NB</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>W</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>P</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><img src="cr_images/_trans.gif" width="10" height="10"></td>
    <td width="168" valign="bottom" bgcolor="#FFFFFF" class="txt_black_10"><img src="cr_images/_trans.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td align="right" bgcolor="#FFFFFF" class="txt_black_10"><img src="cr_images/_trans.gif" width="10" height="10"></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblEXT("cr_byes")%></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblEXT("cr_legbyes")%></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblEXT("cr_noballs")%></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblEXT("cr_wides")%></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblEXT("cr_penalty")%></td>
    <td width="25" align="center" bgcolor="#000000" class="txt_white_10"><strong><%=totExtras%></strong></td>
    <td width="168" bgcolor="#FFFFFF" class="txt_black_10"><img src="cr_images/_trans.gif" width="10" height="10"></td>
  </tr>
</table>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="1"></td>
  </tr>
</table>

<% end if %>

<%
inningsTotalRuns = totRuns + totExtras
%>

<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td align="right" valign="bottom" bgcolor="#FFFFFF" class="txt_black_10"><strong>TOTAL RUNS</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#990000" class="txt_white_10"><strong><%=inningsTotalRuns%></strong></td>
    <td width="168" valign="bottom" bgcolor="#FFFFFF" class="txt_black_10"><img src="cr_images/_trans.gif" width="10" height="10"></td>
  </tr>
</table>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="1"></td>
  </tr>
</table>

<%
queryFOW="select tFOW.*, "& _
		"tPLA.cr_player as vBatsman "& _
		"from cr_score_fow tFOW "& _
		"inner join cr_players tPLA on tPLA.cr_id = tFOW.cr_batsman_ID "& _
		"where (tFOW.cr_fixture_ID='"& fixID &"' and tFOW.cr_team_bat_ID='"& teamsArr(teamsCnt) &"' and cr_inning='"& inningsArr(inningsCnt) &"')"

set dbtblFOW=connect.execute(queryFOW)

if not dbtblFOW.eof then
%>

<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td align="right" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>FALL OF WICKETS </strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>W</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>TT</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>O</strong></td>
  </tr>
<%
do until dbtblFOW.eof
%>
  <tr>
    <td align="right" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblFOW("vBatsman")%></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblFOW("cr_wicket")%></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblFOW("cr_teamtotal")%></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblFOW("cr_overs")%></td>
  </tr>
<%
dbtblFOW.movenext
loop
end if
%>
</table>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="1"></td>
  </tr>
</table>

<%
queryBOW="select tBOW.*, "& _
		"tPLA.cr_player as vBowler "& _
		"from cr_score_bowling tBOW "& _
		"inner join cr_players tPLA on tPLA.cr_id = tBOW.cr_player_ID "& _
		"where (tBOW.cr_fixture_ID='"& fixID &"' and tBOW.cr_team_bat_ID='"& teamsArr(teamsCnt) &"' and cr_inning='"& inningsArr(inningsCnt) &"')"

set dbtblBOW=connect.execute(queryBOW)

if not dbtblBOW.eof then
%>

<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td bgcolor="#CCCCCC" class="txt_black_10"><strong>BOWLING</strong></td>
  </tr>
</table>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="1"></td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td align="left" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>BOWLER</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>O</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>M</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>R</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>W</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>NB</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>WB</strong></td>
    <td width="25" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>DB</strong></td>
    <td width="40" align="center" valign="bottom" bgcolor="#F2F2F2" class="txt_black_10"><strong>ECON</strong></td>
  </tr>
  
<%
dim totBalls, strLen, vECON

do until dbtblBOW.eof

	strLen=len(formatnumber(dbtblBOW("cr_overs"),1))

	if cdbl(dbtblBOW("cr_overs"))=0 then
		vECON = 0
	else
		totBalls = cint(mid(formatnumber(dbtblBOW("cr_overs"),1) , 1, strLen-2)*6) + cint(mid(formatnumber(dbtblBOW("cr_overs"),1) , strLen))
		vECON = formatnumber(((cdbl(dbtblBOW("cr_runs")) / cdbl(totBalls)) * 6),2)
	end if
%>

  <tr>
    <td align="left" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblBOW("vBowler")%></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblBOW("cr_overs")%></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblBOW("cr_maidens")%></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblBOW("cr_runs")%></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblBOW("cr_wickets")%></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblBOW("cr_noball")%></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblBOW("cr_wide")%></td>
    <td width="25" align="center" bgcolor="#FFFFFF" class="txt_black_10"><%=dbtblBOW("cr_dead")%></td>
    <td width="40" align="center" bgcolor="#F2F2F2" class="txt_black_10"><strong><%=vECON%></strong></td>
  </tr>
  
<%
dbtblBOW.movenext
loop
%>

</table>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="1"></td>
  </tr>
</table>

<%
end if
%>

<%
end if

	inningsCnt=inningsCnt+1
	loop

teamsCnt=teamsCnt+1
%>

<% if teamsCnt=2 then %>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#FFFFFF"><img src="cr_images/_trans.gif" width="10" height="5"></td>
  </tr>
  <tr>
    <td bgcolor="#000000"><img src="cr_images/_trans.gif" width="10" height="5"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><img src="cr_images/_trans.gif" width="10" height="5"></td>
  </tr>
</table>
<% end if %>

<%
loop
%>

</body>
</html>
