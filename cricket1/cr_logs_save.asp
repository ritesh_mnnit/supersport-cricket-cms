<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<% option explicit %>

<!-- #Include file="__inc_dbase.asp" -->

<% openConnection %>

<%
dim selID, curID
dim dbtblLOGS, queryLOGS
dim dbtbl, query

selID=request.querystring("selID")

if not isnumeric(selID) or isempty(selID) then
	selID=0
end if
%>

<%
queryLOGS = ""
queryLOGS = queryLOGS & "select "
queryLOGS = queryLOGS & "tLOG.cr_id as vID, "
queryLOGS = queryLOGS & "tLOG.cr_logtype_ID as vTYPEID "
queryLOGS = queryLOGS & "from cr_logs tLOG "
queryLOGS = queryLOGS & "where (tLOG.cr_tournament_ID = "& selID &") "

set dbtblLOGS = connect.execute(queryLOGS)

do until dbtblLOGS.eof

	curID = dbtblLOGS("vID")
	
	query = ""
	query = query & "update cr_logs set "
	query = query & "cr_logtype_ID="& request("f_logtype_"& curID) &", "
	
	if isnumeric(request("f_group_pos_"& dbtblLOGS("vTYPEID"))) then
		query = query & "cr_group_pos="& request("f_group_pos_"& dbtblLOGS("vTYPEID")) &", "
	end if
	
	if isnumeric(request("f_pos_"& curID)) then
		query = query & "cr_pos="& request("f_pos_"& curID) &", "
	end if
	
	if not request("f_team_"& curID) = "" then
		query = query & "cr_team_ID="& request("f_team_"& curID) &", "
	end if
	
	query = query & "cr_P='"& request("f_P_"& curID) &"', "
	query = query & "cr_W='"& request("f_W_"& curID) &"', "
	query = query & "cr_L='"& request("f_L_"& curID) &"', "
	
	query = query & "cr_D='"& request("f_D_"& curID) &"', "
	
	query = query & "cr_T='"& request("f_T_"& curID) &"', "
	query = query & "cr_NR='"& request("f_NR_"& curID) &"', "
	query = query & "cr_BAT='"& request("f_BAT_"& curID) &"', "
	query = query & "cr_BOWL='"& request("f_BOWL_"& curID) &"', "
	query = query & "cr_PEN='"& request("f_PEN_"& curID) &"', "
	query = query & "cr_MATCH='"& request("f_MATCH_"& curID) &"', "
	query = query & "cr_PTS='"& request("f_PTS_"& curID) &"', "
	query = query & "cr_NRR='"& request("f_NRR_"& curID) &"' "
	query = query & "where cr_id=" & curID

	set dbtbl=connect.execute(query)

dbtblLOGS.movenext
loop

query = ""
query = query & "delete from cr_lognotes where cr_tournament_ID="& selID
set dbtbl=connect.execute(query)

query = ""
query = query & "insert into cr_lognotes (cr_tournament_ID,cr_notes) values ("& selID &",'"& replace(request("f_lognotes"),"'","''") &"')"
set dbtbl=connect.execute(query)

closeConnection

response.redirect("cr_logs.asp?selID="& selID)
%>