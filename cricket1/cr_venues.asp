<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<% option explicit %>

<!-- #Include file="__inc_uniquestr.asp" -->
<!-- #Include file="__inc_container.asp" -->

<% setUniqueString %>

<%
dim selID, selVenue, dbtbl, query
selID=request.querystring("selID")

if not isnumeric(selID) then
	selID=0
end if
%>

<%
if not request("btn_process") = "Save" then
%>

<% buildTop %>

<form name="form_venues" method="post">

  <table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
      <td bgcolor="#F2F2F2" class="text_black_12"><strong>VENUES</strong></td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
	  <select name="f_tourlist" class="input_list" id="f_tourlist" style="width:300px" onChange="MM_goToURL('parent','cr_venues.asp?selID='+ f_tourlist.value +'&us=<%=session("s_ustr")%>');return document.MM_returnValue">
	
		<option selected value=""></option>
	
		<%
		query="select cr_id, cr_venue from cr_venues where (not cr_id=1) and (cr_owner="& session("s_owner") &") order by cr_venue"
		set dbtbl=connect.execute(query)
		
		do until dbtbl.eof
		%>
			<% if cint(dbtbl("cr_id")) = cint(selID) then %>
				<option selected value="<%=dbtbl("cr_id")%>"><%=dbtbl("cr_venue")%></option>
			<% else %>
				<option value="<%=dbtbl("cr_id")%>"><%=dbtbl("cr_venue")%></option>
			<% end if %>
		<%
		dbtbl.movenext
		loop
		%>
	
      </select>
	  </td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
  </table>
  <table  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="80" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td bgcolor="#FFFFFF"><input name="btn_create" type="button" class="button_blue" value="Create new" onClick="MM_goToURL('parent','cr_resource_create.asp?resource=venues&us=<%=session("s_ustr")%>');return document.MM_returnValue"></td>
      </tr>
    </table></td>
	<%
	query="select * from cr_venues where cr_id=" & cint(selID)
	set dbtbl=connect.execute(query)
	
	if not dbtbl.eof then
	%>
	<td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    <td><table width="80" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td bgcolor="#FFFFFF"><input name="btn_process" type="submit" class="button_yellow" value="Save"></td>
      </tr>
    </table></td>
    <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    <td><table width="80" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td bgcolor="#FFFFFF"><input name="btn_delete" type="button" class="button_orange" value="Delete" onclick="confirmDeleteResource(<%=selID%>,'venues')"></td>
      </tr>
    </table></td>
	<% end if %>
  </tr>
</table>

<%
if not dbtbl.eof then
	dim vActive
	if dbtbl("cr_active")=1 then
		vActive="checked"
	end if
%>

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>ID NUMBER</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10"><%=dbtbl("cr_id") %></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>VENUE NAME</strong></td>
    <td height="20" bgcolor="#FFFFFF"><input name="f_venue" type="text" class="input_edit_left" id="f_venue" value="<%=dbtbl("cr_venue") %>" maxlength="250"></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>SHORT NAME</strong></td>
    <td height="20" bgcolor="#FFFFFF"><input name="f_venue_sn" type="text" class="input_edit_left" id="f_venue_sn" value="<%=dbtbl("cr_venue_sn") %>" maxlength="50"></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>ACTIVE</strong></td>
    <td height="20" bgcolor="#FFFFFF"><input name="f_active" type="checkbox" id="f_active" value="1" <%=vActive%>></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>LAST UPDATED</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10"><%=dbtbl("cr_lastupdated") %></td>
  </tr>
</table>

<% end if %>

</form>

<% buildBottom %>

<%
else

	if request("f_venue")="" then
		response.write "MISSING DATA"
	else
		dim vlastUpdated, vVenue, vShortname
		vlastUpdated=formatdatetime(now,vblongdate) & " at " & formatdatetime(now,vbshorttime) & " by " & session("s_fullname")
		
		vVenue=replace(request("f_venue"),"'","`")
		vShortname=replace(request("f_venue_sn"),"'","`")
		
		query="update cr_venues set cr_venue='"& vVenue &"', cr_venue_sn='"& vShortname &"', cr_active="& cint(request("f_active")) &", cr_lastupdated='"& vLastUpdated &"' where cr_id="& request("f_tourlist")
		set dbtbl=connect.execute(query)
		
		response.redirect "cr_venues.asp?selID="& request("f_tourlist") & "&us=" & session("s_ustr")
	end if

end if
%>