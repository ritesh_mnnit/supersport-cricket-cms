<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<% option explicit %>

<!-- #Include file="__inc_uniquestr.asp" -->
<!-- #Include file="__inc_container.asp" -->

<% setUniqueString %>

<%
dim dbtbl, query, dateStart, dateEnd, arrYesNo(2), teamID, enableCreate
teamID=request.querystring("teamID")
enableCreate=0

if (not isnumeric(teamID)) or (teamID="") then
	teamID=0
end if

arrYesNo(1)="No"
arrYesNo(2)="Yes"

if not isdate(dateStart) then dateStart=date end if
if not isdate(dateEnd) then dateEnd=date end if
%>

<% buildTop %>

  <table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
      <td bgcolor="#F2F2F2" class="text_black_12"><strong>PLAYERS</strong></td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
  </table>
  <table  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td class="text_black_10">
	  <select name="f_teamlist" class="input_list" id="f_teamlist" style="width:300px" onChange="MM_goToURL('parent','cr_players.asp?teamID='+ f_teamlist.value +'&us=<%=session("s_ustr")%>');return document.MM_returnValue">
	
		<option selected value="">Select a team to begin</option>
	
		<%
		query="select cr_id, cr_team from cr_teams where ((not cr_id=1) and (cr_active=1) and (cr_owner="& session("s_owner") &")) order by cr_team"
		set dbtbl=connect.execute(query)
		
		do until dbtbl.eof
		%>
			<% if cint(dbtbl("cr_id")) = cint(teamID) then %>
				<% enableCreate=1 %>
				<option selected value="<%=dbtbl("cr_id")%>"><%=dbtbl("cr_team")%></option>
			<% else %>
				<option value="<%=dbtbl("cr_id")%>"><%=dbtbl("cr_team")%></option>
			<% end if %>
		<%
		dbtbl.movenext
		loop
		%>
	
      </select>
      </td>
    </tr>
  </table>
  
  <% if enableCreate=1 then %>
  
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
  </table>
  <table  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="80" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td bgcolor="#FFFFFF"><input name="btn_create" type="button" class="button_blue" value="New player" onClick="MM_goToURL('parent','cr_players_create.asp?teamID=<%=teamID%>&us=<%=session("s_ustr")%>');return document.MM_returnValue"></td>
      </tr>
    </table></td>
  </tr>
  </table>

  <% end if %>


<%
query="select tPLA.* from cr_players tPLA where cr_team_ID="& teamID &" order by cr_player"
set dbtbl=connect.execute(query)

if not dbtbl.eof then
%>

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="1" cellspacing="1" bgcolor="#CCCCCC">

<%
do until dbtbl.eof
	
	dim vActive, rowCol
	rowCol="#FFFFFF"
	if dbtbl("cr_active")=0 then
		rowCol="#FCFDDF"
	end if
%>

  <tr align="center" bgcolor="<%=rowCol%>" class="text_black_10">
    <td rowspan="2" bgcolor="<%=rowCol%>"><table width="50"  border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td width="50%" bgcolor="<%=rowCol%>"><input name="btn_edit" type="button" class="button_yellow" id="btn_edit" value="E" onClick="MM_goToURL('parent','cr_players_edit.asp?playerID=<%=dbtbl("cr_id")%>&teamID=<%=teamID%>&us=<%=session("s_ustr")%>');return document.MM_returnValue"></td>
        <td width="50%" bgcolor="<%=rowCol%>"><input name="btn_delete" type="button" class="button_orange" id="btn_delete" value="D" onClick="confirmDeletePlayer(<%=dbtbl("cr_id")%>,'<%=teamID%>')"></td>
      </tr>
    </table>
    </td>
    <td bgcolor="#F2F2F2"><strong>PLAYER NAME</strong></td>
    <td bgcolor="#F2F2F2"><strong>ACTIVE ?</strong></td>
  </tr>
  <tr align="center" bgcolor="#FFFFFF" class="text_black_10">
    <td bgcolor="<%=rowCol%>"><%=dbtbl("cr_player")%></td>
    <td bgcolor="<%=rowCol%>"><%=arrYesNo(dbtbl("cr_active")+1)%></td>
  </tr>
  <tr align="center" bgcolor="#CCCCCC" class="text_black_10">
    <td colspan="3"><img src="cr_images/_trans.gif" width="10" height="1"></td>
  </tr>
  
<%
dbtbl.movenext
loop
%>
  
</table>

<% else %>

  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
  </table>
  <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#000000">
    <tr align="center">
      <td colspan="2" align="left" bgcolor="#FFFFFF" class="text_red_10">No players available</td>
    </tr>
  </table>

<% end if %>

<% buildBottom %>