<% sub buildMainMenu %>

<table width="150" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td bgcolor="#E8E8E8">	<table width="100%" border="0" cellspacing="0" cellpadding="1">
      <% if session("s_permission")="0" then %>
      <tr>
        <td class="text_black_10"><strong>ADMIN OPTIONS</strong></td>
      </tr>
      <tr>
        <td class="text_grey_10"><a href="cr_tournaments.asp?us=<%=session("s_ustr")%>" class="text_grey_10">Tournaments</a></td>
      </tr>
      <tr>
        <td class="text_grey_10"><a href="cr_matchtypes.asp?us=<%=session("s_ustr")%>" class="text_grey_10">Match types</a></td>
      </tr>
      <tr>
        <td class="text_grey_10"><a href="cr_gametypes.asp?us=<%=session("s_ustr")%>" class="text_grey_10">Game types</a></td>
      </tr>
      <tr>
        <td class="text_grey_10"><a href="cr_sponsors.asp?us=<%=session("s_ustr")%>" class="text_grey_10">Sponsors</a></td>
      </tr>
      <tr>
        <td class="text_grey_10"><a href="cr_venues.asp?us=<%=session("s_ustr")%>" class="text_grey_10">Venues</a></td>
      </tr>
      <tr>
        <td class="text_grey_10"><a href="cr_teams.asp?us=<%=session("s_ustr")%>" class="text_grey_10">Teams</a></td>
      </tr>
      <tr>
        <td class="text_grey_10"><a href="cr_players.asp?us=<%=session("s_ustr")%>" class="text_grey_10">Players</a></td>
      </tr>
	  <tr>
        <td class="text_grey_10"><a href="cr_dismissals.asp?us=<%=session("s_ustr")%>" class="text_grey_10">Dismissal types</a></td>
      </tr>
	  <tr>
        <td class="text_grey_10"><a href="cr_logs.asp?us=<%=session("s_ustr")%>" class="text_grey_10">Logs</a></td>
      </tr>
      <tr>
        <td class="text_black_10"><strong>MAINTENANCE</strong></td>
      </tr>
	  <tr>
        <td class="text_grey_10"><a href="cr_ignore_tournaments.asp?us=<%=session("s_ustr")%>" class="text_grey_10">Disable Tournament Feed</a></td>
      </tr>
      <% end if %>
      <tr>
        <td class="text_black_10"><strong>GENERAL OPTIONS</strong></td>
      </tr>
      <tr>
        <td class="text_grey_10"><a href="cr_fixtures.asp?us=<%=session("s_ustr")%>" class="text_grey_10">Fixtures</a></td>
      </tr>
      <tr>
        <td class="text_black_10"><strong>PERSONAL OPTIONS</strong></td>
      </tr>
      <tr>
        <td class="text_grey_10"><a href="cr_password.asp?us=<%=session("s_ustr")%>" class="text_grey_10">Change my password</a></td>
      </tr>
    </table></td>
  </tr>
</table>

<% end sub %>