<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>


<% option explicit %>

<!-- #Include file="__inc_uniquestr.asp" -->
<!-- #Include file="__inc_container.asp" -->

<% 
setUniqueString 
buildTop 
%>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">

<%
function curPageURL()
 dim s, protocol, port

 if Request.ServerVariables("HTTPS") = "on" then 
   s = "s"
 else 
   s = ""
 end if  
 
 protocol = strleft(LCase(Request.ServerVariables("SERVER_PROTOCOL")), "/") & s 

 if Request.ServerVariables("SERVER_PORT") = "80" then
   port = ""
 else
   port = ":" & Request.ServerVariables("SERVER_PORT")
 end if  

 curPageURL = protocol & "://" & Request.ServerVariables("SERVER_NAME") &_ 
              port & Request.ServerVariables("SCRIPT_NAME")
end function

function strLeft(str1,str2)
 strLeft = Left(str1,InStr(str1,str2)-1)
end function
%>

$.extend($.expr[":"], 
{
	"containsIN": function(elem, i, match, array) 
	{
		return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
	}
});

function getValue()
{
	var x = $('#input').val();

	if(!(!x))
		{
			x = x.toLowerCase();
			$("#tournaments > tbody > tr").each(function(){ $(this).hide();});  
			var s = $("#tournaments > tbody > tr:has(strong)").find("strong:containsIN("+ x +")").parent().parent().show();
		}
		else
		{
			$("#tournaments > tbody > tr").each(function(){ $(this).show();});
		}
}



 
  

</script>
<form name="form_ignore" method="post">

  <table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
      <td bgcolor="#F2F2F2" class="text_black_12"><strong>FIXTURES TO BE IGNORED</strong></td>
	  <td bgcolor="#F2F2F2">
		<strong class="text_black_12">SEARCH:&nbsp;</strong><input type="text" text="Search" id="input" onkeyup="getValue()"/>
	  </td>
	  <td  bgcolor="F2F2F2" align="right" width="12%" >
	  <input type="submit" name="test" value="Ignore Checked" />
	  </td>
    </tr>
  </table>
 

  <table id="tournaments" width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
<% 
 dim dbtblSeries 
 set dbtblSeries = connect.execute("SELECT distinct a.Series COLLATE DATABASE_DEFAULT As Series FROM Cricket.dbo.scorecard_matches a EXCEPT SELECT b.Name COLLATE DATABASE_DEFAULT From Cricket.dbo.ignore_tournaments b ORDER BY Series")
'set dbtblSeries = connect.execute("SELECT distinct a.Series COLLATE DATABASE_DEFAULT FROM Cricket.dbo.scorecard_matches a LEFT JOIN Cricket.dbo.ignore_tournaments b ON a.Series COLLATE DATABASE_DEFAULT = b.Name COLLATE DATABASE_DEFAULT where a.Series COLLATE DATABASE_DEFAULT is not ''")

 do until dbtblSeries.eof
 %>
 

	<tr>
      <td bgcolor="#F2F2F2" class="text_black_12"><strong><%=dbtblSeries("Series")%></strong></td>
	  <td bgcolor="#F2F2F2">
	  <input type="checkbox" name='group1' id="grp1" value="<%=dbtblSeries("Series")%>">
	  </td>
    </tr>
<%
dbtblSeries.movenext
loop

%>
  </table>
<%
dim Rows
dim aRows
dim i
Rows =  Request.Form("group1")
if Rows <> "" then
	aRows = Split(Rows,",")

	dim alertMessage, tour
	alertMessage = "The following tournaments have been added to the ignore list:\n"
	
	For i = lBound(aRows) to uBound(aRows)
	tour = Trim(cStr(aRows(i)))
	tour = Replace(tour,"''","''''")
	alertMessage = alertMessage & "\n\u2022" & Trim(cStr(aRows(i)))
	connect.execute("INSERT INTO Cricket.dbo.ignore_tournaments (Name) VALUES ('" & tour & "');")
	next
	
	Response.Write("<script>alert(" & chr(34) & alertMessage & chr(34) & ");window.location.replace('" & curPageURL() & "');</script>")

end if

%>
</form>



<% buildBottom %>