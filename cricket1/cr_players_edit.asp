<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<% option explicit %>

<!-- #Include file="__inc_uniquestr.asp" -->
<!-- #Include file="__inc_container.asp" -->

<% setUniqueString %>

<%
dim dbtbl, query, teamID, playerID, arrYesNo(2)

teamID=request.querystring("teamID")
playerID=request.querystring("playerID")

arrYesNo(1)="No"
arrYesNo(2)="Yes"

if (not isnumeric(playerID)) or (not isnumeric(teamID)) then
	response.redirect "_error.asp"
end if
%>

<%
if not request("btn_process") = "Save" then
%>

<% buildTop %>

<form name="form_fixture" method="post">

  <table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
      <td bgcolor="#F2F2F2" class="text_black_12"><strong>EDIT PLAYER</strong></td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
  </table>
  <table  border="0" cellspacing="0" cellpadding="0">
    <tr>

      <td><table width="80" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
          <tr>
            <td bgcolor="#FFFFFF"><input name="btn_process" type="submit" class="button_yellow" value="Save"></td>
          </tr>
      </table></td>

      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
      <td><table width="80" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
        <tr>
          <td bgcolor="#FFFFFF"><input name="btn_back" type="button" class="button_green" id="btn_back" value="Back" onClick="history.back()"></td>
        </tr>
      </table></td>
    </tr>
  </table>
<%
query="select tPLA.*, tTEA.cr_team AS vTeam "& _
	  "from cr_players tPLA, cr_teams tTEA "& _
	  "where ((tPLA.cr_id="& playerID &") and (tTEA.cr_id = tPLA.cr_team_ID))"

set dbtbl=connect.execute(query)

if not dbtbl.eof then
	dim active
%>

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>ID NUMBER</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10"><%=dbtbl("cr_id") %></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>TEAM NAME</strong></td>
    <td height="20" bgcolor="#FFFFFF"><select name="f_team" class="input_list" id="f_team" style="width:300px">
	<option selected value="<%=dbtbl("cr_team_ID")%>"><%=dbtbl("vTeam")%></option>
	<% createTeams %>
    </select></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>PLAYER NAME</strong></td>
    <td height="20" bgcolor="#FFFFFF"><input name="f_player" type="text" class="input_edit_left" id="f_player" value="<%=dbtbl("cr_player") %>" maxlength="100"></td>
  </tr>
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>ACTIVE ?</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10"><select name="f_active" class="input_list" id="f_active">
	<option selected value="<%=dbtbl("cr_active")%>"><%=arrYesNo(dbtbl("cr_active")+1)%></option>
	<% createYesNo %>
    </select></td>
  </tr>
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>LAST UPDATED</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10"><%=dbtbl("cr_lastupdated") %></td>
  </tr>
</table>

  <% else %>
        
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
</table>
  <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#000000">
    <tr align="center">
      <td colspan="2" align="left" bgcolor="#FFFFFF" class="text_red_10">No data</td>
    </tr>
  </table>

<% end if %>

</form>

<% buildBottom %>

<%
else
	
	if  (request("f_team")="" or _
		request("f_player")="") then
		
		response.write "DETAILS ERROR<br>make sure you have values in all fields."
	else
	
		dim vlastUpdated, vPlayer
		vlastUpdated=formatdatetime(now,vblongdate) & " at " & formatdatetime(now,vbshorttime) & " by " & session("s_fullname")
		
		vPlayer=replace(request("f_player"),"'","`")
		
		query="update cr_players set cr_team_ID="& request("f_team") & _
									", cr_player='"& vPlayer & _
									"', cr_active="& request("f_active") & _
									", cr_lastupdated='"& vLastUpdated & _
									"' where cr_id="& playerID

		set dbtbl=connect.execute(query)

		response.redirect "cr_players.asp?teamID="& request("f_team") & "&us=" & session("s_ustr")

	end if

end if
%>





