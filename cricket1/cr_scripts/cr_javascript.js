function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function confirmDeleteResource(docid,resource)
{
	if(confirm("This action cannot be undone!\n\nAre you sure you want to delete this entry?"))
		{
			MM_goToURL('parent','cr_resource_delete.asp?docid='+ docid + '&resource='+ resource);
			return document.MM_returnValue;
		}
}

function confirmDeleteFixture(docid,dateStart,dateEnd)
{
	if(confirm("This action cannot be undone!\n\nAre you sure you want to delete this fixture?"))
		{
			MM_goToURL('parent','cr_fixtures_delete.asp?docid='+ docid +'&dateStart='+ dateStart +'&dateEnd='+ dateEnd);
			return document.MM_returnValue;
		}
	
}

function confirmDeletePlayer(playerID,teamID)
{
	if(confirm("This action cannot be undone!\n\nAre you sure you want to delete this player?"))
		{
			MM_goToURL('parent','cr_players_delete.asp?playerID='+ playerID +'&teamID='+ teamID);
			return document.MM_returnValue;
		}
	
}

function verifyTime(timePart)
	{
		if(isNaN(event.srcElement.value))
			{
				alert(event.srcElement.value + " is not a valid entry");
				event.srcElement.value="";
			}
		else
			{
				var tpValid = true;
				
				switch(timePart)
					{
						case 'h': if(event.srcElement.value > 23) { tpValid = false; } break;
						case 'm': if(event.srcElement.value > 59) { tpValid = false; } break;
					}
					
				if(!tpValid)
					{
						alert(event.srcElement.value + " is not a valid entry");
						event.srcElement.value="";
					}
			}
	}

function verifyInt()
	{
		if(isNaN(event.srcElement.value) || event.srcElement.Key == 32)
			{
				alert(event.srcElement.value + " is not a valid entry");
				event.srcElement.value="";
			}
	}

function resetDate(src)
	{
		src.selectedIndex = -1;
	}

function isDate(sDate)
	{
		var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/
		if (re.test(sDate))
			{
				var dArr = sDate.split("/");
				var d = new Date(sDate);
				return d.getMonth() + 1 == dArr[0] && d.getDate() == dArr[1] && d.getFullYear() == dArr[2];
			}
				else
					{
						return false;
					}
	}

function verifyDate(y,m,d, checkFuture)
	{
		var year = y , month = m , day = d;

		if((month != "")&&(day != "")&&(year != ""))
			{
				var displayDate = year + "-" + month + "-" + day; // USE THIS FORMAT TO DISPLAY THE DATE CORRECTLY
				var checkDate = month + "/" + day + "/" + year; // USE THIUS FORMAT TO USE isDate() FUNCTION
				
				if(!isDate(checkDate))
					{
						alert("The specified date [" + displayDate + "] is not valid !");
						resetDate(event.srcElement);
					}
				else
					{
						if(checkFuture)
							{
								var shiftDate = new Date(dateStr);
								var todayDate = new Date();
							
								if(todayDate.valueOf() < shiftDate.valueOf())
									{
										alert("The date you specified is in the future !");
										resetDate(event.srcElement);
									}
							}
					}
			}
	}
	