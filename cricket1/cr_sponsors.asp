<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<% option explicit %>

<!-- #Include file="__inc_uniquestr.asp" -->
<!-- #Include file="__inc_container.asp" -->

<% setUniqueString %>

<%
dim selID, selTeam, dbtbl, query
selID=request.querystring("selID")

if not isnumeric(selID) then
	selID=0
end if
%>

<%
if not request("btn_process") = "Save" then
%>

<% buildTop %>

<form name="form_sponsors" method="post">

  <table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
      <td bgcolor="#F2F2F2" class="text_black_12"><strong>SPONSORS</strong></td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
	  <select name="f_sponsorlist" class="input_list" id="f_sponsorlist" style="width:300px" onChange="MM_goToURL('parent','cr_sponsors.asp?selID='+ f_sponsorlist.value +'&us=<%=session("s_ustr")%>');return document.MM_returnValue">
	
		<option selected value=""></option>
	
		<%
		query="select cr_id, cr_sponsor from cr_sponsors where (not cr_id=1) and (cr_owner="& session("s_owner") &") order by cr_sponsor"
		set dbtbl=connect.execute(query)
		
		do until dbtbl.eof
		%>
			<% if cint(dbtbl("cr_id")) = cint(selID) then %>
				<option selected value="<%=dbtbl("cr_id")%>"><%=dbtbl("cr_sponsor")%></option>
			<% else %>
				<option value="<%=dbtbl("cr_id")%>"><%=dbtbl("cr_sponsor")%></option>
			<% end if %>
		<%
		dbtbl.movenext
		loop
		%>
	
      </select>
	  </td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
  </table>
  <table  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="80" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td bgcolor="#FFFFFF"><input name="btn_create" type="button" class="button_blue" value="Create new" onClick="MM_goToURL('parent','cr_resource_create.asp?resource=sponsors&us=<%=session("s_ustr")%>');return document.MM_returnValue"></td>
      </tr>
    </table></td>
	<%
	query="select * from cr_sponsors where cr_id=" & cint(selID)
	set dbtbl=connect.execute(query)
	
	if not dbtbl.eof then
	%>
	<td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    <td><table width="80" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td bgcolor="#FFFFFF"><input name="btn_process" type="submit" class="button_yellow" value="Save"></td>
      </tr>
    </table></td>
    <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    <td><table width="80" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td bgcolor="#FFFFFF"><input name="btn_delete" type="button" class="button_orange" value="Delete" onclick="confirmDeleteResource(<%=selID%>,'sponsors')"></td>
      </tr>
    </table></td>
	<% end if %>
  </tr>
</table>

<%
if not dbtbl.eof then
	dim vActive
	if dbtbl("cr_active")=1 then
		vActive="checked"
	end if
%>

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>ID NUMBER</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10"><%=dbtbl("cr_id") %></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>SPONSOR</strong></td>
    <td height="20" bgcolor="#FFFFFF"><input name="f_sponsor" type="text" class="input_edit_left" id="f_sponsor" value="<%=dbtbl("cr_sponsor") %>" maxlength="250"></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>SHORT NAME</strong></td>
    <td height="20" bgcolor="#FFFFFF"><input name="f_sponsor_sn" type="text" class="input_edit_left" id="f_sponsor_sn" value="<%=dbtbl("cr_sponsor_sn") %>" maxlength="50"></td>
  </tr>
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>LOGO URL</strong></td>
    <td height="20" bgcolor="#FFFFFF"><input name="f_logourl" type="text" class="input_edit_left" id="f_logourl" value="<%=dbtbl("cr_logo_url") %>" maxlength="250"></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>ACTIVE</strong></td>
    <td height="20" bgcolor="#FFFFFF"><input name="f_active" type="checkbox" id="f_active" value="1" <%=vActive%>></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>LAST UPDATED</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10"><%=dbtbl("cr_lastupdated") %></td>
  </tr>
</table>

<% end if %>

</form>

<% buildBottom %>

<%
else

	if request("f_sponsor")="" then
		response.write "MISSING DATA"
	else
		dim vlastUpdated, vSponsor, vShortname
		vlastUpdated=formatdatetime(now,vblongdate) & " at " & formatdatetime(now,vbshorttime) & " by " & session("s_fullname")
		
		vSponsor=replace(request("f_sponsor"),"'","`")
		vShortname=replace(request("f_sponsor_sn"),"'","`")
		
		query="update cr_sponsors set cr_sponsor='"& vSponsor &"', cr_sponsor_sn='"& vShortname &"', cr_logo_url='"& request("f_logourl") &"', cr_active="& cint(request("f_active")) &", cr_lastupdated='"& vLastUpdated &"' where cr_id="& request("f_sponsorlist")
		set dbtbl=connect.execute(query)
		
		response.redirect "cr_sponsors.asp?selID="& request("f_sponsorlist") & "&us=" & session("s_ustr")
	end if

end if
%>