<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<% option explicit %>

<!-- #Include file="__inc_uniquestr.asp" -->
<!-- #Include file="__inc_container.asp" -->

<% setUniqueString %>

<%
dim selID, selTournament, dbtbl, query
selID=request.querystring("selID")

if not isnumeric(selID) then
	selID=0
end if
%>

<%
if not request("btn_process") = "Save" then
%>

<% buildTop %>

<form name="form_tournaments" method="post">

  <table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
      <td bgcolor="#F2F2F2" class="text_black_12"><strong>TOURNAMENTS</strong></td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
	  <select name="f_tourlist" class="input_list" id="f_tourlist" style="width:300px" onChange="MM_goToURL('parent','cr_tournaments.asp?selID='+ f_tourlist.value +'&us=<%=session("s_ustr")%>');return document.MM_returnValue">
	
		<option selected value=""></option>
	
		<%
		query="select cr_id, cr_tournament from cr_tournaments where (not cr_id=1) and (cr_owner="& session("s_owner") &") order by cr_tournament"
		set dbtbl=connect.execute(query)
		
		do until dbtbl.eof
		%>
			<% if cint(dbtbl("cr_id")) = cint(selID) then %>
				<option selected value="<%=dbtbl("cr_id")%>"><%=dbtbl("cr_tournament")%></option>
			<% else %>
				<option value="<%=dbtbl("cr_id")%>"><%=dbtbl("cr_tournament")%></option>
			<% end if %>
		<%
		dbtbl.movenext
		loop
		%>
	
      </select>
	  </td>
    </tr>
  </table>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    </tr>
  </table>
  <table  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="80" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td bgcolor="#FFFFFF"><input name="btn_create" type="button" class="button_blue" value="Create new" onClick="MM_goToURL('parent','cr_resource_create.asp?resource=tournaments&us=<%=session("s_ustr")%>');return document.MM_returnValue"></td>
      </tr>
    </table></td>
	<%
	'query="select * from cr_tournaments where cr_id=" & cint(selID)
	
	query="select tTOU.*, tSPO.cr_id AS vSponsorID, tSPO.cr_sponsor AS vSponsor from cr_tournaments tTOU, cr_sponsors tSPO where tSPO.cr_id=tTOU.cr_sponsor_ID and tTOU.cr_id=" & cint(selID)
	
	set dbtbl=connect.execute(query)
	
	if not dbtbl.eof then
	%>
	<td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    <td><table width="80" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td bgcolor="#FFFFFF"><input name="btn_process" type="submit" class="button_yellow" value="Save"></td>
      </tr>
    </table></td>
    <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
    <td><table width="80" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      <tr>
        <td bgcolor="#FFFFFF"><input name="btn_delete" type="button" class="button_orange" value="Delete" onclick="confirmDeleteResource(<%=selID%>,'tournaments')"></td>
      </tr>
    </table></td>
	<% end if %>
  </tr>
</table>

<%
if not dbtbl.eof then
	dim vActive
	if dbtbl("cr_active")=1 then
		vActive="checked"
	end if
	
	dim vInternational
	if dbtbl("cr_international")=1 then
		vInternational="checked"
	end if
%>

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="cr_images/_trans.gif" width="10" height="10"></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>ID NUMBER</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10"><%=dbtbl("cr_id") %></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>TOURNAMENT</strong></td>
    <td height="20" bgcolor="#FFFFFF"><input name="f_tournament" type="text" class="input_edit_left" id="f_tournament" value="<%=dbtbl("cr_tournament") %>" maxlength="250"></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>SHORT NAME</strong></td>
    <td height="20" bgcolor="#FFFFFF"><input name="f_shortname" type="text" class="input_edit_left" id="f_shortname" value="<%=dbtbl("cr_tournament_sn") %>" maxlength="50"></td>
  </tr>
  <% if session("s_owner")=0 then %>
  <tr>
  	<td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>TOURNAMENT URL</strong></td>
  	<td height="20" bgcolor="#FFFFFF"><input name="f_url" type="text" class="input_edit_left" id="f_url" value="<%=dbtbl("cr_tournament_URL") %>" maxlength="50"></td>
  </tr>
  <tr>
  	<td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>INTERNATIONAL</strong></td>
  	<td height="20" bgcolor="#FFFFFF"><input name="f_international" type="checkbox" id="f_international" value="1" <%=vInternational%>></td>
  </tr>
  <% end if %>
  <tr>
    <td height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>SPONSOR</strong></td>
    <td height="20" bgcolor="#FFFFFF"><select name="f_sponsor" class="input_list" id="f_sponsor">
	<option selected value="<%=dbtbl("vSponsorID")%>"><%=dbtbl("vSponsor")%></option>
	<% createSponsors %>
    </select></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>ACTIVE</strong></td>
    <td height="20" bgcolor="#FFFFFF"><input name="f_active" type="checkbox" id="f_active" value="1" <%=vActive%>></td>
  </tr>
  <tr>
    <td width="100" height="20" align="left" bgcolor="#F2F2F2" class="text_black_10"><strong>LAST UPDATED</strong></td>
    <td height="20" bgcolor="#FFFFFF" class="text_black_10"><%=dbtbl("cr_lastupdated") %></td>
  </tr>
</table>

<% end if %>

</form>

<% buildBottom %>

<%
else

	if request("f_tournament")="" then
		response.write "MISSING DATA"
	else
		dim vlastUpdated, vTournament, vShortname, vTournamentURL
		vlastUpdated=formatdatetime(now,vblongdate) & " at " & formatdatetime(now,vbshorttime) & " by " & session("s_fullname")
		
		vTournament=replace(request("f_tournament"),"'","`")
		vShortname=replace(request("f_shortname"),"'","`")
		vTournamentURL=replace(request("f_url"),"'","`")
		
		query="update cr_tournaments set cr_tournament='"& vTournament &"', cr_tournament_sn='"& vShortname &"', cr_tournament_URL='"& vTournamentURL &"', cr_sponsor_ID='"& request("f_sponsor") &"', cr_international="& cint(request("f_international")) &", cr_active="& cint(request("f_active")) &", cr_lastupdated='"& vLastUpdated &"' where cr_id="& request("f_tourlist")
		set dbtbl=connect.execute(query)
		
		response.redirect "cr_tournaments.asp?selID="& request("f_tourlist") & "&us=" & session("s_ustr")
	end if

end if
%>