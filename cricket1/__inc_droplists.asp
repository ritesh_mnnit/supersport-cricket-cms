<% sub createSponsors %>

	<option value="1"></option>

	<%
	dim querySponsors, dbtblSponsors
	querySponsors="select cr_id, cr_sponsor from cr_sponsors where (cr_active=1) and (cr_owner="& session("s_owner") &") order by cr_sponsor"
	set dbtblSponsors=connect.execute(querySponsors)
		
	do until dbtblSponsors.eof
	%>

		<option value="<%=dbtblSponsors("cr_id")%>"><%=dbtblSponsors("cr_sponsor")%></option>
	
	<%
	dbtblSponsors.movenext
	loop
	%>

<% end sub %>

<% sub createTournaments %>

	<option value=""></option>

	<%
	dim queryTournaments, dbtblTournaments
	queryTournaments="select cr_id, cr_tournament from cr_tournaments where (cr_active=1) and (cr_owner="& session("s_owner") &") order by cr_tournament"
		
'		response.write(queryTournaments)
'		response.End()
		
	set dbtblTournaments=connect.execute(queryTournaments)
		
	do until dbtblTournaments.eof
	%>

		<option value="<%=dbtblTournaments("cr_id")%>"><%=dbtblTournaments("cr_tournament")%></option>
	
	<%
	dbtblTournaments.movenext
	loop
	%>

<% end sub %>

<% sub createGametypes %>

	<option value="1"></option>

	<%
	dim queryGametypes, dbtblGametypes
	queryGametypes="select cr_id, cr_gametype from cr_gametypes where (cr_active=1) and (cr_owner="& session("s_owner") &") order by cr_gametype"
	set dbtblGametypes=connect.execute(queryGametypes)
		
	do until dbtblGametypes.eof
	%>

		<option value="<%=dbtblGametypes("cr_id")%>"><%=dbtblGametypes("cr_gametype")%></option>
	
	<%
	dbtblGametypes.movenext
	loop
	%>

<% end sub %>

<% sub createMatchtypes %>

	<option value="1"></option>

	<%
	dim queryMatchtypes, dbtblMatchtypes
	queryMatchtypes="select cr_id, cr_matchtype from cr_matchtypes where (cr_active=1) and (cr_owner="& session("s_owner") &") order by cr_matchtype"
	set dbtblMatchtypes=connect.execute(queryMatchtypes)
		
	do until dbtblMatchtypes.eof
	%>

		<option value="<%=dbtblMatchtypes("cr_id")%>"><%=dbtblMatchtypes("cr_matchtype")%></option>
	
	<%
	dbtblMatchtypes.movenext
	loop
	%>

<% end sub %>

<% sub createVenues %>

	<option value="1"></option>

	<%
	dim queryVenues, dbtblVenues
	queryVenues="select cr_id, cr_venue from cr_venues where (cr_active=1) and (cr_owner="& session("s_owner") &") order by cr_venue"
	set dbtblVenues=connect.execute(queryVenues)
		
	do until dbtblVenues.eof
	%>

		<option value="<%=dbtblVenues("cr_id")%>"><%=dbtblVenues("cr_venue")%></option>
	
	<%
	dbtblVenues.movenext
	loop
	%>

<% end sub %>

<% sub createChannels %>

<option value="1"></option>

<%
dim connectGen
set connectGen = Server.CreateObject("ADODB.Connection")
connectGen.open("PROVIDER=SQLOLEDB;SERVER=sql.dstvo.local;UID=supersport;PWD=s3@fs#sd2;DATABASE=soccer")

dim queryChannels, dbtblChannels
queryChannels="SELECT ShortName FROM soccer.dbo.Channels WHERE (Active = 1) ORDER BY ShortName"
set dbtblChannels=connectGen.execute(queryChannels)
		
do until dbtblChannels.eof
%>

	<option value="<%=dbtblChannels("ShortName")%>"><%=dbtblChannels("ShortName")%></option>
	
<%
dbtblChannels.movenext
loop
connectGen.close
set connectGen = nothing
%>

<% end sub %>

<% sub createTeams %>

	<option value="1"></option>

	<%
	dim queryTeams, dbtblTeams
	queryTeams="select cr_id, cr_team from cr_teams where (cr_active=1) and (cr_owner="& session("s_owner") &") order by cr_team"
	set dbtblTeams=connect.execute(queryTeams)
		
	do until dbtblTeams.eof
	%>

		<option value="<%=dbtblTeams("cr_id")%>"><%=dbtblTeams("cr_team")%></option>
	
	<%
	dbtblTeams.movenext
	loop
	%>

<% end sub %>

<% sub createYesNo %>

<option value="1">Yes</option>
<option value="0">No</option>

<% end sub %>

<% sub createDays %>

<%
dim cnt , str
cnt = 1
do until cnt = 32

if cnt < 10 then str = "0" & cnt else str = cnt end If
%>

<option value="<%=str%>"><%=str%></option>

<%
cnt = cnt + 1
loop
%>

<% end sub %>

<% sub createMonths %>

<%
dim cnt
cnt = 0
do until cnt = 12
cnt = cnt + 1
%>

<option value="<%=cnt%>"><%=monthname(cnt)%></option>

<%
loop
%>

<% end sub %>

<% sub createYears %>

<%
dim listyear,cnt

cnt = 0
listyear = Year(Date) -1

do until cnt = 5

cnt = cnt + 1
%>

<option value="<%=listyear%>"><%=listyear%></option>

<%
listyear = listyear + 1
loop
%>

<% end sub %>

<% sub createTime(part) %>

<%
dim cnt , str, maxCnt
cnt = 0
maxCnt=0

if part="hrs" then maxCnt=24 end if
if part="min" then maxCnt=60 end if
if part="sec" then maxCnt=60 end if

do until cnt = maxCnt

if cnt < 10 then str = "0" & cnt else str = cnt end if
%>

<option value="<%=str%>"><%=str%></option>

<%
cnt = cnt + 1
loop
%>

<% end sub %>